﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(MeshRenderer))]
public class CellSelector : Editor
{
	public override void OnInspectorGUI(){
	}
//	{
//		if (Selection.activeGameObject != null)
//		{
//			GameObject obj = Selection.activeGameObject;
//			CellConstructor cell = TryToFindCellParent(obj.transform);
//			if (cell != null)
//			{
//				Selection.activeGameObject = cell.gameObject;
//			}
//		}
//		base.DrawDefaultInspector();
//	}

	public CellConstructor TryToFindCellParent(Transform parent)
	{
		if (parent != null && parent.transform.parent != null)
		{
			CellConstructor cell = parent.transform.parent.GetComponent<CellConstructor>();
			if (cell != null)
			{
				return cell;
			}
			return TryToFindCellParent(parent.transform.parent);
		}
		return null;
	}
	void Update()
	{
		Debug.Log("Update");
	}
}
