﻿using UnityEngine;
using System.Collections;

public class WinnerMenuUI : BaseAnimationUI
{

	protected override void OnShow()
	{
		Game.isPaused = true;
	}
	
	protected override void OnHide()
	{
	}

	public void NextLevel()
	{
		Player.info.level += 1;
		Player.Save();
		BaseUI.AsyncShowWithLoaderUI<GameUI, LoaderUI>(0, () =>
		{
			Game.Restart();
		});
	}
	
	public void BackToMainMenu()
	{
		BaseUI.AsyncShowWithLoaderUI<MainMenuUI, LoaderUI>(0, null, "MainMenu");
	}
}
