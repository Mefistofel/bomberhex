﻿using UnityEngine;
using System.Collections;

public class PauseMenuUI : BaseAnimationUI
{
	protected override void OnShow()
	{
		Game.isPaused = true;
	}

	protected override void OnHide()
	{
		Game.isPaused = false;
	}
	
	public void BackToMainMenu()
	{
		BaseUI.AsyncShowWithLoaderUI<MainMenuUI, LoaderUI>(0, null, "MainMenu");
	}

	public void RestartLevel()
	{
		// hehe
		BaseUI.AsyncShowWithLoaderUI<GameUI, LoaderUI>(0, () =>
		{
			Game.Restart();
		});
	}

	public void ContinueGame()
	{
		BaseUI.Show<GameUI>();
	}

	void Update()
	{
		if (Input.GetKeyUp(KeyCode.Escape))
		{
			ContinueGame();
		}
		if (Input.GetKeyUp(KeyCode.Joystick1Button6)|| Input.GetKeyUp(KeyCode.Joystick1Button7))
		{
			ContinueGame();
		}
	}
}
