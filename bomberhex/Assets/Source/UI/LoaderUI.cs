﻿using UnityEngine;
using System.Collections;

public class LoaderUI : BaseUI
{
	[SerializeField]
	GameObject baseElement = null;
	const int elementWidth = 5;
	const int elementHeight = 9;
	public RectTransform[,] elements = new RectTransform[elementWidth, elementHeight];
	
	protected override void Awake()
	{
		if (baseElement != null)
		{
			for (int i = 0; i < elementWidth; i++)
			{
				for (int j = 0; j < elementHeight; j++)
				{
					elements[i, j] = ((GameObject)Instantiate(baseElement)).GetComponent<RectTransform>();
					if (elements[i, j] != null)
					{
						elements[i, j].SetParent(this.transform);
						if (j % 2 == 0)
						{
							elements[i, j].anchoredPosition = new Vector2(292f * 2f * ((float)i - (float)elementWidth / 2f), 172f * 2f * ((float)j / 2f - (float)elementHeight / 4f));
						}
						else
						{
							elements[i, j].anchoredPosition = new Vector2(292f * 2f * ((float)i + 0.5f - (float)elementWidth / 2f), 172f * 2f * ((float)j / 2f - (float)elementHeight / 4f));
						}
					}
				}
			}
		}
		baseElement.SetActive(false);
		base.Awake();
	}

	void SetState(float anim, bool forward = true)
	{
		
		for (int i = 0; i < elementWidth; i++)
		{
			for (int j = 0; j < elementHeight; j++)
			{
				float size;
				if (forward)
				{
					size = Mathf.Max(0f, Mathf.Min(1f, anim * 4.5f - 1f  - (float)i / (float)elementWidth - (float)j / (float)elementHeight * 0.5f ));
				}
				else
				{
					size = Mathf.Max(0f, Mathf.Min(1f, anim * 4.5f - 1f  - (float)(elementWidth - i) / (float)elementWidth - (float)(elementHeight - j) / (float)elementHeight * 0.5f ));
				}
				if (elements[i, j] != null)
				{
					elements[i, j].transform.localScale = Vector3.one * size;
				}
			}
		}
	}

	protected override void Show(OnUIFinishLoading onFinishShowing)
	{
		SetState(0);
		Timer.Add(0.8f, (anim) =>
		{
			SetState(1f - (1f - anim) * (1f - anim));
		},
		() =>
		{
			SetState(1);
			if (onFinishShowing != null)
			{
				onFinishShowing();
			}
		});
	}
	
	protected override void Hide(OnUIFinishLoading onFinishHiding)
	{
		Timer.Add(1.2f, (anim) =>
		{
			SetState(1f - anim * anim, false);
		},
		() =>
		{
			SetState(0, false);
			if (onFinishHiding != null)
			{
				onFinishHiding();
			}
		});
	}

}
