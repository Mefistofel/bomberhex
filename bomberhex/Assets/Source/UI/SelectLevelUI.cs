﻿using UnityEngine;
using System.Collections;

public class SelectLevelUI : BaseUI
{	
	public void StartLevel(int levelNum)
	{
//		Debug.Log("Start level num " + levelNum.ToString());
		BaseUI.AsyncShowWithLoaderUI<GameUI, LoaderUI>(0, null, "GameScene");
	}
	
	public void BackToMainMenu()
	{
		BaseUI.Show<MainMenuUI>();
	}
}
