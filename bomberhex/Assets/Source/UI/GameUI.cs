﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameUI: BaseUI// : BaseAnimationUI
{
	static GameUI instance = null;

	[SerializeField]
	LifeCountController	lifesController = null; // set from editor

	[SerializeField]
	GameObject plantBombButton = null; // Set from editor - button for bomb plant on mobile devices

	[SerializeField]
	LabelFaderEffectController levelLabel = null;

	[SerializeField]
	VirtualJoystick joystick = null;
	
	[SerializeField]
	Transform stick = null; // Set from editor

	[SerializeField]
	Transform joystickBase = null; // Set from editor

	Vector2 joystickDirection = Vector2.zero;
	const float minimalJoystickAccuracy = 0.25f;
	float[] stickDirectionAngles = new float[7];

	float[] defaultStickDirectionAngles = new float[] {
		0f,
		60f,
		120f,
		180f,
		240f,
		300f,
		0f
	};
	// direction angles between each neighbor
	//     \   /
	//      \ /
	//  ---- 0 ----
	//      / \
	//     /   \

	public static void SetLifeCount(int lifeCount = 0)
	{
		if (instance != null && instance.lifesController != null)
		{
			instance.lifesController.SetLifeCount(lifeCount);
		}
	}

	public static void SetStickRotation(float angle)
	{
		if (instance != null)
		{
			for (int i = 0; i < instance.stickDirectionAngles.Length; i++)
			{
				instance.stickDirectionAngles[i] = RoundAngle(instance.defaultStickDirectionAngles[i] + angle);
				if (instance.stick != null)
				{
					instance.stick.transform.localEulerAngles = new Vector3(0, 0, angle);
				}
				if (instance.joystickBase != null)
				{
					instance.joystickBase.transform.localEulerAngles = new Vector3(0, 0, angle);
				}
			}
		}
	}

	protected override void Awake()
	{
		instance = this;
		stickDirectionAngles = defaultStickDirectionAngles;
		base.Awake();
	}

	void Start()
	{
		SetStickRotation(CameraController.Angle);
		// kostil
		Timer.Add(1f, () => {
			if (!IsShowed)
			{
				Debug.Log("Game UI Hack");
				OnShow();
			}
		});
	}

	protected override void OnShow()
	{
		if (plantBombButton != null)
		{
			// if touch platforms - save button
			if (Application.platform == RuntimePlatform.Android ||
				Application.platform == RuntimePlatform.IPhonePlayer ||
				Application.platform == RuntimePlatform.WP8Player ||
			    Application.platform == RuntimePlatform.WindowsEditor ||
			    Application.platform == RuntimePlatform.OSXEditor)
			{
				plantBombButton.SetActive(true);
			}
			else
			{
				plantBombButton.SetActive(false);
			}
		}
		if (levelLabel != null)
		{
			levelLabel.ShowEffect("level " + (Player.info.level + 1).ToString());
		}
		SetStickRotation(CameraController.Angle);
	}
	
	public void PauseGame()
	{
		BaseUI.Show<PauseMenuUI>();
	}
	
	public void PlantBomb() // for touch platforms
	{
		Game.PlantBomb();
	}
	
	void Update()
	{
		// joystick
		// A - KeyCode.Joystick1Button0
		// B - KeyCode.Joystick1Button1
		// X - KeyCode.Joystick1Button2
		// Y - KeyCode.Joystick1Button3
		// LShift - KeyCode.Joystick1Button4
		// RShift - KeyCode.Joystick1Button5
		// Back - KeyCode.Joystick1Button6
		// Start - KeyCode.Joystick1Button7
		if (Input.GetKeyUp(KeyCode.Joystick1Button6) || Input.GetKeyUp(KeyCode.Joystick1Button7))
		{
			PauseGame();
		}
		if (Input.GetKeyUp(KeyCode.Escape))
		{
			PauseGame();
		}
		if (Input.GetKeyDown(KeyCode.Joystick1Button0))
		{
			PlantBomb();
		}
		if (Input.GetKeyUp(KeyCode.Space))
		{
			PlantBomb();
		}
		if (!Game.currentHero.IsHeroMoving)
		{
			joystickDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
			if (joystickDirection.sqrMagnitude > 1)
			{
				joystickDirection.Normalize();
			}
			int direction = FindDirection(joystickDirection);
			if (direction == 0 && joystick != null)
			{
				direction = FindDirection(joystick.touchDelta);
			}
			if (direction > 0)
			{
				Game.currentHero.MoveToPoint(Game.currentHero.coord + Map.neighbors[(13 - (direction - 1)) % 6]);
			}
		}
	}

	int FindDirection(Vector2 direction)
	{
		// small direction change
		if (direction.sqrMagnitude < minimalJoystickAccuracy * minimalJoystickAccuracy)
		{
			return 0;
		}
		float angle = Utils.GetAngle(new Vector2(direction.y, direction.x));
		for (int i = 0; i < stickDirectionAngles.Length - 1; i++)
		{
			if (stickDirectionAngles[i] < stickDirectionAngles[i + 1])
			{
				if ((angle >= stickDirectionAngles[i] && angle < stickDirectionAngles[i + 1]))
				{
					return i + 1;
				}
			}
			else
			{
				if (!(angle < stickDirectionAngles[i] && angle >= stickDirectionAngles[i + 1]))
				{
					return i + 1;
				}
			}
		}
		return 0;
	}

	static float RoundAngle(float angle)
	{
		if (angle < 0)
		{
			angle += 360f;
		}
		if (angle >= 360f)
		{
			angle -= 360f;
		}
		return angle;
	}
}
