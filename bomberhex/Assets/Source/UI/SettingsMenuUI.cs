﻿using UnityEngine;
using System.Collections;

public class SettingsMenuUI : BaseAnimationUI
{
	[SerializeField]GameObject soundOn = null;
	[SerializeField]GameObject soundOff = null;
	[SerializeField]GameObject musicOn = null;
	[SerializeField]GameObject musicOff = null;
	
	protected override void OnShow()
	{
		SetButtonState();
	}

	void SetButtonState()
	{
		if (soundOn != null)
		{
			soundOn.SetActive(Settings.SoundEnabled);
		}
		if (soundOff != null)
		{
			soundOff.SetActive(!Settings.SoundEnabled);
		}
		if (musicOn != null)
		{
			musicOn.SetActive(Settings.MusicEnabled);
		}
		if (musicOff != null)
		{
			musicOff.SetActive(!Settings.MusicEnabled);
		}
	}

	public void SoundSwitch()
	{
		Settings.SoundEnabled = !Settings.SoundEnabled;
		Settings.Save();
		SetButtonState();
	}
	
	public void MusicSwitch()
	{
		Settings.MusicEnabled = !Settings.MusicEnabled;
		Settings.Save();
		SetButtonState();
	}
	
	public void EraseProgress()
	{
		Player.info = new Player();
		Player.Save();
	}
	
	public void BackToMainMenu()
	{
		BaseUI.Show<MainMenuUI>();
	}
}
