﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuUI : BaseAnimationUI
{

	[SerializeField]Text titleObject = null;
	[SerializeField]Text continueGame = null;
	Vector2 defaultPosition = Vector2.zero;

	static bool firstTimeShow = true;

	protected override void OnShow()
	{
		if (titleObject != null)
		{
			RectTransform title = titleObject.GetComponent<RectTransform>();
			defaultPosition = title.anchoredPosition;
			titleObject.color = new Color(0,0,0,0);
			if (firstTimeShow)
			{
				Timer.Add(-0.8f, 0.2f, (anim) =>
				{
					if (titleObject != null)
					{
						title.anchoredPosition = defaultPosition + new Vector2((1f - anim) * (1f - anim) * 40f, 0);
						titleObject.color = new Color(0,0,0,anim);
					}
				},
				()=>
				{
					if (titleObject != null)
					{
						title.anchoredPosition = defaultPosition;
						titleObject.color = new Color(0,0,0,1f);
						firstTimeShow = false;
					}
				});
			}
			else
			{
				Timer.Add(-0.3f, 0.2f, (anim) =>
				          {
					if (titleObject != null)
					{
						title.anchoredPosition = defaultPosition + new Vector2((1f - anim) * (1f - anim) * 40f, 0);
						titleObject.color = new Color(0,0,0,anim);
					}
				},
				()=>
				{
					if (titleObject != null)
					{
						title.anchoredPosition = defaultPosition;
						titleObject.color = new Color(0,0,0,1f);
					}
				});
			}
		}
		// TODO kill continue label
		if (continueGame != null)
		{
//			if (Player.info.level == 0)
//			{
				continueGame.gameObject.SetActive(false);
//			}
//			else
//			{
//				continueGame.gameObject.SetActive(true);
//			}
		}
	}

	public void ContinueGame()
	{
		BaseUI.AsyncShowWithLoaderUI<GameUI, LoaderUI>(0, null, "GameScene");
	}

	public void StartNewGame()
	{
//		Player.info = new Player();
//		Player.Save();
//		BaseUI.AsyncShowWithLoaderUI<GameUI, LoaderUI>(0, null, "GameScene");
		BaseUI.Show<SelectLevelUI>();
	}
	
	public void ShowSettingsMenu()
	{
		BaseUI.Show<SettingsMenuUI>();
	}

	public void ExitGame()
	{
		Application.Quit();
	}
}
