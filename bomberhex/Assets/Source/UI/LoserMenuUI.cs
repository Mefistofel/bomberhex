﻿using UnityEngine;
using System.Collections;

public class LoserMenuUI : BaseAnimationUI
{

	protected override void OnShow()
	{
		Game.isPaused = true;
	}
	
	protected override void OnHide()
	{
	}
	
	public void Restart()
	{
		BaseUI.AsyncShowWithLoaderUI<GameUI, LoaderUI>(0, () =>
		{
			Game.Restart();
		});
	}

	public void BackToMainMenu()
	{
		BaseUI.AsyncShowWithLoaderUI<MainMenuUI, LoaderUI>(0, null, "MainMenu");
	}
	
	// Update is called once per frame
//	void Update () {
//	
//	}
}
