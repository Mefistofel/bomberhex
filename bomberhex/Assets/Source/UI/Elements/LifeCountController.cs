﻿using UnityEngine;
using System.Collections;

public class LifeCountController : MonoBehaviour
{
	[SerializeField]
	GameObject[] lifeMarkers = null; // fill from editor

	int count = -1;
	bool[] isShowed = null;

	public int Count
	{
		get
		{
			return count;
		}
		set
		{
			count = value;
		}
	}

	public void SetLifeCount(int lifeCount = 0)
	{
		if (isShowed == null)
		{
			isShowed = new bool[lifeMarkers.Length];
			for (int i = 0; i < isShowed.Length; i++)
			{
				isShowed[i] = false;
				if (lifeMarkers[i] != null)
				{
					lifeMarkers[i].transform.localScale = Vector3.zero;
					lifeMarkers[i].SetActive(true);
				}
			}
		}
		for (int i = 0; i < lifeMarkers.Length; i++)
		{
			if (lifeMarkers[i] != null)
			{
				bool needShow = (i < lifeCount);
				if (isShowed[i] != needShow)
				{
					isShowed[i] = needShow;
					GameObject temp = lifeMarkers[i];
					if (isShowed[i])
					{
						Timer.Add(-(float)i * 0.1f, 0.2f, (anim) =>
						{
							if (temp != null)
							{
								temp.transform.localScale = Vector3.one * anim;
							}
						},
						() =>
						{
							if (temp != null)
							{
								temp.transform.localScale = Vector3.one;
							}
						});
					}
					else
					{
						Timer.Add(-(float)i * 0.1f, 0.2f, (anim) =>
						{
							if (temp != null)
							{
								temp.transform.localScale = Vector3.one * (1f - anim);
							}
						},
						() =>
						{
							if (temp != null)
							{
								temp.transform.localScale = Vector3.zero;
							}
						});
					}
				}
			}
		}
	}
}
