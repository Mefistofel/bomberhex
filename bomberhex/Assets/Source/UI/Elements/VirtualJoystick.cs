﻿using UnityEngine;
using System.Collections;

// virtual joystic for touch platforms
// handle user touches
// work with some base sprite on it and stick sprite in children
public class VirtualJoystick : MonoBehaviour
{
	[SerializeField]
	RectTransform stickObject = null; // set from editor
	
	public Vector2 touchDelta = Vector2.zero;

	Vector2 defaultPosition = Vector2.zero;
	Vector2 needPosition = Vector2.zero;
	Vector3 startPosition = Vector3.zero;
	RectTransform selfTransform = null;

	int touchId = -1; // >0 - is touched

	void Start()
	{
		selfTransform = GetComponent<RectTransform>();
		if (selfTransform != null)
		{
			defaultPosition = selfTransform.anchoredPosition;
		}
	}

	Vector2 convertToUISpace(Vector2 touchPosition)
	{
		Vector2 finalPosition = new Vector2();
		RectTransformUtility.ScreenPointToLocalPointInRectangle(selfTransform, touchPosition, null, out finalPosition);
		return finalPosition;
	}

	void Update()
	{
		if (Input.touchSupported)
		{
			if (Input.touchCount > 0)
			{
				foreach(var touch in Input.touches)
				{
					// TODO refactor
					if (touch.phase == TouchPhase.Began && (touch.position.x < Screen.height / 3f) && (touch.position.y < Screen.width / 5f))
					{
						startPosition = Input.mousePosition;
						needPosition = convertToUISpace(startPosition) + new Vector2(200, 200);
						selfTransform.anchoredPosition = needPosition;
						touchId = touch.fingerId;
					}
				}
			}
			if (Input.touchCount == 0 || Input.touches[touchId].phase == TouchPhase.Canceled || Input.touches[touchId].phase == TouchPhase.Ended)
			{
				touchId = -1;
				touchDelta = Vector2.zero;
			}
		}
		else
		{ // mouse control
			if (Input.GetMouseButtonDown(0))
			{
				startPosition = Input.mousePosition;
				// TODO refactor
				if (startPosition.x < Screen.height / 3f && startPosition.y < Screen.width / 5)
				{
					needPosition = convertToUISpace(startPosition) + new Vector2(200, 200);
					selfTransform.anchoredPosition = needPosition;
					touchId = 0;
				}
			}
			if (Input.GetMouseButtonUp(0))
			{
				touchId = -1;
				touchDelta = Vector2.zero;
			}
		}

		if (touchId >= 0)
		{
			touchDelta = (Input.mousePosition - startPosition) * (1f / (float)Screen.height) * 20f;
			if (stickObject != null)
			{
				Vector2 stickShift = touchDelta * 0.05f * 1024f;
				if (stickShift.magnitude > 100f)
				{
					stickShift *= 1f / stickShift.magnitude * 100f;
				}
				stickObject.anchoredPosition = stickShift;
			}
		}
		else
		{ // return joystick to start
			selfTransform.anchoredPosition = defaultPosition;
			if (stickObject != null)
			{
				stickObject.anchoredPosition = Vector2.zero;
			}
		}
	}

	#if UNITY_EDITOR
	void OnDrawGizmos()
	{
	}
	#endif
}
