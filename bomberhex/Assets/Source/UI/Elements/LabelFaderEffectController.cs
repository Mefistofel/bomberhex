﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LabelFaderEffectController : MonoBehaviour
{
	[SerializeField]
	Text levelNum = null; // fill from editor

	[SerializeField]
	float startTime = 1.0f;
	[SerializeField]
	float showTime = 2.0f;

	public void ShowEffect(string text)
	{
		if (levelNum != null)
		{
			levelNum.text = text;//"level " + (Player.info.level + 1).ToString();
			levelNum.color = new Color(1,1,1, 0);
			Vector2 defaultPosition = levelNum.rectTransform.anchoredPosition;
			Timer.Add(-startTime, 0.5f, (anim) =>
			{
				if (levelNum != null)
				{
					levelNum.rectTransform.anchoredPosition = defaultPosition + new Vector2((1f - anim) * (1f - anim) * 200f, 0);
					levelNum.color = new Color(1,1,1,anim);
				}
			},
			() =>
			{
				if (levelNum != null)
				{
					levelNum.rectTransform.anchoredPosition = defaultPosition;
					levelNum.color = new Color(1,1,1,1f);
					Timer.Add(-showTime, 0.5f, (anim) =>
					{
						if (levelNum != null)
						{
							levelNum.rectTransform.anchoredPosition = defaultPosition + new Vector2((anim) * (anim) * 200f, 0);
							levelNum.color = new Color(1,1,1,1f - anim);
						}
					},
					() =>
					{
						if (levelNum != null)
						{
							levelNum.rectTransform.anchoredPosition = defaultPosition;
							levelNum.color = new Color(1,1,1,0f);
						}
					});
				}
			});
		}
	}
}
