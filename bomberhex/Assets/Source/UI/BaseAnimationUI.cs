﻿using UnityEngine;
using System.Collections;

public class BaseAnimationUI : BaseUI
{

	RectTransform selfTransform = null;
	Vector2 finalPosition = Vector2.zero;

	protected override void Awake()
	{
		selfTransform = GetComponent<RectTransform>();
		finalPosition = selfTransform.anchoredPosition;
	}

	protected override void Show(OnUIFinishLoading onFinishShowing)
	{
		selfTransform.anchoredPosition = finalPosition + new Vector2(700f, 0);
		Timer.Add(0.3f, (anim) =>
		{
			selfTransform.anchoredPosition = finalPosition + new Vector2((1f - anim) * (1f - anim) * 700f, 0);
		},
		() =>
		{
			selfTransform.anchoredPosition = finalPosition;
			if (onFinishShowing != null)
			{
				onFinishShowing();
			}
		});
	}

	protected override void Hide(OnUIFinishLoading onFinishHiding)
	{
		Timer.Add(0.3f, (anim) =>
		{
			selfTransform.anchoredPosition = finalPosition + new Vector2((anim * anim) * 500f, 0);
		},
		() =>
		{
			selfTransform.anchoredPosition = finalPosition + new Vector2(500f, 0);
			if (onFinishHiding != null)
			{
				onFinishHiding();
			}
		});
	}
}
