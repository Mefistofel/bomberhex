﻿using UnityEngine;
using System.Collections;

//EnableAndDisableMusic on this object
public class MusicController : MonoBehaviour {

	[SerializeField]AudioSource source = null; // set from editor

	void Start () {
		if (source == null)
		{
			source = GetComponent<AudioSource>();
		}
		SwitchMusic();
	}

	public void SwitchMusic()
	{
		if (source != null)
		{
			source.enabled = Settings.MusicEnabled;
		}
	}
}
