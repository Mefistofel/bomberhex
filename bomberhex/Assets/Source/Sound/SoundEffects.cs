using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class SoundEffects : MonoBehaviour {

	public static SoundEffects instance;
	public static SoundClip Button;
	public static SoundClip Move;
	public static SoundClip Switch;
	
	static AudioSource source;
	
	public class SoundClip {
		AudioClip clip;
		static AudioSource source;
		public AudioSource audioSource{
			get{
				return source;
			}
		}
		public SoundClip (AudioClip audioClip, AudioSource audioSource) {
			clip = audioClip;
			source = audioSource;
		}
		public void PlayThread(float scale = 1f) {
			source.clip = clip;
			source.volume = scale;
			if (source.enabled) {
				source.Play ();
			}
		}
		public void Play (float scale = 1f) {
			if (Settings.SoundEnabled) {
				source.PlayOneShot (clip, scale);
			}	
		}
	}

	void Start () {
		if (instance != null) {
			Destroy(this.gameObject);
			return;
		}
		instance = this;
		Button	 		= new SoundClip(Resources.Load("Sounds/button") as AudioClip, GetComponent<AudioSource>());
		Move	 		= new SoundClip(Resources.Load("Sounds/move") as AudioClip, GetComponent<AudioSource>());
		Switch	 		= new SoundClip(Resources.Load("Sounds/switch") as AudioClip, GetComponent<AudioSource>());
		DontDestroyOnLoad (this.gameObject);
	}
}
