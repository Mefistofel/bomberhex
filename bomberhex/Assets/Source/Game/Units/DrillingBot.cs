﻿using UnityEngine;
using System.Collections;

public class DrillingBot : Unit
{
	[SerializeField]
	Animator controledAnimator = null;
	[SerializeField]
	Animator headActionAnimator = null;
	[SerializeField]
	ParticleSystem[] particles; // Set from Editor
	[SerializeField]
	AudioSource explosionSound = null;// set from editor
	[SerializeField]
	Transform drillTransform; // Set from Editor
	[SerializeField]
	ParticleSystem drillEmiter; // Set from Editor


	bool drilling = false;

	const float drillSpeed = 360f * 5f;
	
	void Start()
	{
		Init();
	}
	
	//	int phase = 0;
	int needDirection = 0;
	
	bool CanMoveToDirection(int direction)
	{
		return (Map.IsOnFloor(coord + Map.neighbors[direction]) && !Map.IsUnitOnTile(coord + Map.neighbors[direction]));
	}
	
	void CheckAction()
	{
		// 40% drill chance
		if (Random.Range(0, 100) < 40)
		{
			Idle(3f, OnStartDrilling, OnEndDrilling);
			return;
		}
		OnEndDrilling();
		// 20% wait chance
		if (Random.Range(0, 100) < 20)
		{
			Idle(1f);
			return;
		}
		// 80% move forward chance
		if (Random.Range(0, 100) < 80)
		{
			if (CanMoveToDirection(needDirection))
			{
				MoveAnimated(coord + Map.neighbors[needDirection], 1f, OnMoveForward);
				return;
			} else {
				Idle(1.5f);
			}
		}
		// try 5 times to find random free place nearby
		for (int i = 0; i < 5; i++)
		{
			needDirection = Random.Range(0, 6);
			if (CanMoveToDirection(needDirection))
			{
				RotateToDirectionAnimated(needDirection, 0.55f, OnTurnLeft, OnTurnRight);
				MoveAnimated(coord + Map.neighbors[needDirection], 1f, OnMoveForward);
				return;
			}
		}
		Idle(3f, OnStartDrilling);
	}
	
	void Update()
	{
		if (!IsMoving)
		{
			CheckAction();
			// actions for tests
			//			needDirection = (needDirection + 2) % 6;
			//			RotateToDirectionAnimated(needDirection, 0.55f, OnTurnLeft, OnTurnRight);
			//			MoveAnimated(coord + Map.neighbors[needDirection], OnMoveForward, 1f);
		}
		if (drilling && drillTransform != null)
		{
			drillTransform.localRotation = drillTransform.localRotation * Quaternion.Euler(0, Time.deltaTime * drillSpeed, 0);
		}
		UpdateCurrentAction();
	}
	
	protected override void OnPause(bool paused)
	{
		if (controledAnimator != null)
		{
			controledAnimator.speed = paused?0:1;
		}
	}
	
	protected override void DieEffects()
	{
		// effects
		if (particles != null)
		{
			foreach(var emitter in particles)
			{
				if (emitter != null)
				{
					emitter.Play();
				}
			}
		}
		// sounds
		if (explosionSound != null && Settings.SoundEnabled)
		{
			explosionSound.Play();
		}
		Destroy(this);
		Destroy(controledAnimator.gameObject, 0.3f);
		Destroy(gameObject, 2.2f);
	}
	
	void OnStartDrilling()
	{
		if (headActionAnimator != null)
		{
			headActionAnimator.SetBool("drill", true);
			drilling = true;
		}
	}

	void OnEndDrilling()
	{
		if (headActionAnimator != null)
		{
			headActionAnimator.SetBool("drill", false);
			drilling = false;
		}
	}
	
	void OnMoveForward()
	{
		if (controledAnimator != null)
		{
			controledAnimator.Play("move_forward");
		}
	}
	
	void OnTurnLeft()
	{
		if (controledAnimator != null)
		{
			controledAnimator.Play("turn_left");
		}
	}
	
	void OnTurnRight()
	{
		if (controledAnimator != null)
		{
			controledAnimator.Play("turn_right");
		}
	}
}