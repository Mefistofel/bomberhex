﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Unit : MonoBehaviour
{
	// main params
	public Coord coord = Coord.Zero;
	public Coord prevCoord = Coord.Zero;
	public int direction = 0;

	// actions list
	public delegate void UpdateAction(float time);
	public delegate void FlagAction();
	
	public struct SheduledAction
	{
		public float time;
		public UpdateAction updateAction;
		public FlagAction startAction;
		public FlagAction endAction;
	}

	UpdateAction currentAction;
	FlagAction endCurrentAction;
	List<SheduledAction> actionList = new List<SheduledAction>();

	// timer for actions
	float actionTimer = 0;
	float currentActionTime = 1f;
	// for game pause
	bool isPaused = false;

	// default move params
	protected Vector3 moveVector = Vector3.zero;
	Vector3 needPoint = Vector3.zero;

	// default rotate params
	protected float currentAngle = 0;
	protected float needAngle = 0;
	float angleDirection = 0;
	float segmentPart = 0;

	public bool IsMoving
	{
		get { return (actionTimer > 0);}
	}

	public float ActionTimer
	{
		get { return actionTimer / currentActionTime;}
	}

	void Start()
	{
		Init();
	}

	void Update()
	{
		UpdateCurrentAction();
	}

	protected virtual void Init()
	{
		transform.localPosition = Map.CoordToHex(coord);
	}
	
	public void AddAction(float actionTime, UpdateAction onUpdateAction = null, FlagAction onStartAction = null, FlagAction onEndAction = null)
	{
		actionList.Add(new SheduledAction() {time = actionTime, updateAction = onUpdateAction, startAction = onStartAction, endAction = onEndAction});
		if (actionTimer <= 0)
		{
			StartNextAction();
		}
	}

	public void Idle(float animationTime = 1f, FlagAction startAction = null, FlagAction endAction = null)
	{
		AddAction(animationTime, null, startAction, endAction);
	}

	public void MoveAnimated(Coord to, float animationTime = 1f, FlagAction startAction = null, FlagAction endAction = null)
	{
		Map.MoveUnit(this, coord, to);
		prevCoord = coord;
		coord = to;
		needPoint = Map.CoordToHex(coord);
		moveVector = transform.localPosition - needPoint;// Vector3.zero;
		AddAction(animationTime, OnMoveAction, startAction, endAction);
	}

	void OnMoveAction(float actionTime)
	{
		transform.localPosition = needPoint + moveVector * (actionTimer / currentActionTime);
		if (actionTime <= 0)
		{
			Map.RemovePrevUnit(prevCoord);
			prevCoord = coord;
		}
	}

	public void RotateToDirectionAnimated(int newDirection, float turnAnimationTime = 0.5f, FlagAction turnLeftAction = null, FlagAction turnRightAction = null)
	{
		if (direction == newDirection)
		{ // no need to rotate
			return;
		}
		currentAngle = Map.neighborAngle[direction];
		needAngle = Map.neighborAngle[newDirection];

		if (newDirection < direction)
		{
			if (direction - newDirection <= 3)
			{ // turn right
				angleDirection = 60f;
				segmentPart = direction - newDirection;
				for(int i = 0; i < segmentPart; i++)
				{
					AddAction(turnAnimationTime, OnRotateAction, turnLeftAction);
				}
			}
			else
			{ // turn left
				angleDirection = -60;
				segmentPart = 6 + newDirection - direction;
				for(int i = 0; i < segmentPart; i++)
				{
					AddAction(turnAnimationTime, OnRotateAction, turnRightAction);
				}
			}
		}
		else
		{
			if (newDirection - direction <= 3)
			{ // turn left
				angleDirection = -60;
				segmentPart = newDirection - direction;
				for(int i = 0; i < segmentPart; i++)
				{
					AddAction(turnAnimationTime, OnRotateAction, turnRightAction);
				}
			}
			else
			{ // turn right
				angleDirection = 60f;
				segmentPart = 6 + direction - newDirection;
				for(int i = 0; i < segmentPart; i++)
				{
					AddAction(turnAnimationTime, OnRotateAction, turnLeftAction);
				}
			}
		}
		direction = newDirection % 6;
	}
	
	void OnRotateAction(float actionTime)
	{
		currentAngle = needAngle + (segmentPart - 1) * angleDirection + angleDirection * actionTime;
		transform.localEulerAngles = new Vector3(0, currentAngle, 0);
		if (actionTime <= 0)
		{
			segmentPart -= 1f;
		}
	}

	void StartNextAction()
	{
		if (actionList.Count > 0)
		{ // start current action
			StartCurrentAction(actionList[0]);
			actionList.RemoveAt(0);
		}
	}

	void StartCurrentAction(SheduledAction action)
	{
		currentActionTime = action.time;
		actionTimer = action.time;
		currentAction = action.updateAction;
		endCurrentAction = action.endAction;
		if (action.startAction != null)
		{
			action.startAction();
		}
	}

	protected virtual void OnPause(bool paused)
	{

	}

	protected void UpdateCurrentAction()
	{
		if (isPaused != Game.isPaused)
		{
			OnPause(Game.isPaused);
			isPaused = Game.isPaused;
		}
		if (Game.isPaused)
		{
			return;
		}
		if (actionTimer > 0)
		{
			actionTimer -= Time.deltaTime;
			if (actionTimer <= 0)
			{
				actionTimer = 0;
			}
			if (currentAction != null)
			{
				currentAction(actionTimer / currentActionTime);
			}
			if (actionTimer == 0)
			{
				if (endCurrentAction != null)
				{
					endCurrentAction();
				}
				StartNextAction();
			}
		}
		else
		{
			StartNextAction();
		}
	}
	
	protected virtual void DieEffects()
	{
		Destroy(gameObject, 0.2f);
	}
	
	public virtual void OnHit()
	{
		DieEffects();
	}
	
	public void TryHit(Coord blowCoord)
	{
		if (prevCoord == coord) // stay on cell
		{
			if (blowCoord == coord)
			{
				OnHit();
			}
		}
		else
		{ // moved from prev coord to coord
			if (actionTimer / currentActionTime < 0.5f)
			{
				if (blowCoord == coord)
				{
					OnHit();
				}
			}
			else
			{
				if (blowCoord == prevCoord)
				{
					OnHit();
				}
			}
		}
	}
}