using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour
{
	[SerializeField]
	ParticleSystem[] particles;
	[SerializeField]
	ParticleSystem secondaryBlast = null;
	[SerializeField]
	AudioSource explosionSound = null;// set from editor
	[SerializeField]
	AudioSource bipSound = null;// set from editor
	const float defaultExplosiomTime = 2.6f;
	float explodeTimer = defaultExplosiomTime;
	int beepCount = 2;
	Coord coord = Coord.Zero;
	int power = 2;

	// for drop animation
	const float dropTime = 0.5f;
	float dropAnimationTimer = dropTime;
	Vector3 needPosition;
	Vector3 startPosition;

	public static Bomb Plant(Vector3 dropPlace, Coord coord, int power = 2, float timer = defaultExplosiomTime)
	{
		Bomb bomb = Utils.LoadPrefab<Bomb>("Prefabs/Bomb", dropPlace);
		bomb.coord = coord;
		Map.AddBomd(coord, bomb);
		bomb.explodeTimer = timer;
		bomb.power = power;
		bomb.transform.localScale = Vector3.zero;
		return bomb;
	}

	void OnDestroy()
	{
		Game.currentBombCount--;
	}

	void Start()
	{
		if (Map.instance == null)
		{
			return;
		}
		needPosition = Map.CoordToHex(coord);
		startPosition = transform.localPosition;
	}

	void Explode()
	{
		explodeTimer = 0;
		Destroy(gameObject, 1.2f);
		Map.AddBomd(coord, null);
		// sounds
		if (explosionSound != null && Settings.SoundEnabled)
		{
			explosionSound.Play();
		}
		// effects
		if (particles != null)
		{
			foreach (var particle in particles)
			{
				if (particle != null)
				{
					particle.Play();
				}
			}
		}
		Timer.Add(0.2f, (anim) => {
			transform.localScale = Vector3.one * (1f - anim * anim);
		});
		int[] waves = FindWavesLenght();
		for (int side = 0; side < waves.Length; side++)
		{
			for (int len = 0; len < waves[side]; len++)
			{
				float distance = len + 1;
				Vector3 direction = Map.CoordToHex(Map.neighbors[side]) - Map.CoordToHex(Coord.Zero);
				Timer.Add(distance * 0.1f - 0.06f + 0.033f, () =>
				{
					Vector3 smallBlastPosition = transform.localPosition + direction * (distance - 0.66f) + Random.insideUnitSphere * 0.15f;
					secondaryBlast.Emit(smallBlastPosition + Random.insideUnitSphere * 0.45f, Vector3.zero, 0.3f + Random.Range(0, 0.2f), 0.36f, Color.gray);
					secondaryBlast.Emit(smallBlastPosition + Random.insideUnitSphere * 0.45f, Vector3.zero, 0.3f + Random.Range(0, 0.2f), 0.36f, Color.gray);
					secondaryBlast.Emit(smallBlastPosition, Vector3.zero, 1.0f - distance * 0.1f, 0.3f, Color.white);
				});
				Timer.Add(distance * 0.1f - 0.06f + 0.066f, () =>
				{
					Vector3 smallBlastPosition = transform.localPosition + direction * (distance - 0.33f) + Random.insideUnitSphere * 0.1f;
					secondaryBlast.Emit(smallBlastPosition + Random.insideUnitSphere * 0.45f, Vector3.zero, 0.3f + Random.Range(0, 0.2f), 0.36f, Color.gray);
					secondaryBlast.Emit(smallBlastPosition, Vector3.zero, 1.0f - distance * 0.1f, 0.3f, Color.white);
				});
				Timer.Add(distance * 0.1f - 0.06f + 0.1f, () =>
				{
					Vector3 smallBlastPosition = transform.localPosition + direction * distance + Random.insideUnitSphere * 0.05f;
					secondaryBlast.Emit(smallBlastPosition, Vector3.zero, 1.0f - distance * 0.1f, 0.3f, Color.white);
				});
			}
		}
		StressCamera.Stress(1.2f);
	}

	int[] FindWavesLenght()
	{
		int[] waves = new int[6];
		Map.GetCell(coord).OnHit(coord);
		// check all sides for explosion lenght
		for (int side = 0; side < Map.neighbors.Length; side ++)
		{
			waves[side] = 0;
			for (int l = 1; l <= power; l++)
			{
				Coord cellPosition = coord + Map.neighbors[side] * l;
				Map.Cell cell = Map.GetCell(cellPosition);
				cell.OnHit(cellPosition);
				if (cell.type == CellType.Floor)
				{
					waves[side] = l;
					if (cell.bomb != null)
					{
						Timer.Add((float)l * 0.1f - 0.06f + 0.1f, () =>
						{
							cell.bomb.Explode();
						});
					}
				}
				else
				{
					if (cell.type == CellType.TreeWall)
					{
						waves[side] = l;
						Timer.Add((float)l * 0.1f - 0.06f + 0.1f, () =>
						{
							DestroyAnimated(Map.instance.cell[cellPosition.x, cellPosition.y].structureObject);
							Map.instance.cell[cellPosition.x, cellPosition.y].type = CellType.Floor;
							TreeDestructionEffect.EmitLeaves(Map.CoordToHex(cellPosition));
						});
					}
					else
					{
						if (cell.type == CellType.Wall)
						{
							waves[side] = l;
							Timer.Add((float)l * 0.1f - 0.06f + 0.1f, () =>
							{
								DestroyAnimated(Map.instance.cell[cellPosition.x, cellPosition.y].structureObject);
								Map.instance.cell[cellPosition.x, cellPosition.y].type = CellType.Floor;
								WallDestructionEffect.EmitBricks(Map.CoordToHex(cellPosition));
							});
							break;
						}
						break;
					}
				}
			}
		}
		return waves;
	}

	static void DestroyAnimated(GameObject destroyedObject)
	{
		if (destroyedObject != null)
		{
			Transform transform = destroyedObject.transform;
			Timer.Add(0.3f, (anim) =>
			{
				transform.localScale = Vector3.one * (1f - anim * anim);
			},
			() =>
			{
				Destroy(destroyedObject);
			});
		}
	}

	void Update()
	{
		if (Game.isPaused)
		{
			return;
		}
		// drop animation
		if (dropAnimationTimer > 0)
		{
			dropAnimationTimer -= Time.deltaTime;
			float anim = 1f - dropAnimationTimer / dropTime;
			transform.localScale = Vector3.one * Mathf.Min(1f, (anim * 2f)); // scale on start
			transform.localPosition = Vector3.Lerp(startPosition, needPosition, anim) + new Vector3(0, 0.8f - (anim * 2f - 1f) * (anim * 2f - 1f) * 0.8f);
			if (dropAnimationTimer <= 0)
			{
				dropAnimationTimer = 0;
				transform.localScale = Vector3.one;
				transform.localPosition = needPosition;
			}
		}
		else
		{ // explosion timer
			if (explodeTimer > 0)
			{
				explodeTimer -= Time.deltaTime;
				transform.localScale = Vector3.one * Mathf.Max(1f, 1f + Mathf.Sin(explodeTimer * 20f / Mathf.PI) * 0.2f); // pulse animation
				int beep = (int)(explodeTimer + 0.2f);
				if (beepCount != beep )
				{
					beepCount = beep;
					if (bipSound != null && Settings.SoundEnabled)
					{
						bipSound.Play();
					}
				}
				if (explodeTimer <= 0)
				{
					Explode();
				}
			}
		}
	}
}
