﻿using UnityEngine;
using System.Collections;

public class Enemy : BaseUnit
{
	[SerializeField]Animator enemyController; // Set from Editor
	[SerializeField]ParticleSystem[] particles; // Set from Editor
	[SerializeField]
	AudioSource explosionSound = null;// set from editor
	// anim
	bool isRun = false;

	const float moveTime = 1.2f;
	float animationTimer = 0;

	// for rotate
	float angle = 180f;
	float needAngle = 180f;
	const float rotationSpeed = 360f; // degrees per second to rotate for hero


	void Start()
	{
		if (Map.instance == null)
		{
			return;
		}
		animationTimer = moveTime * Random.Range(0, 1f);
		needPoint = Map.CoordToHex(coord);
		transform.localPosition = needPoint;
	}

	Coord[] directionShift = new Coord[]
	{
		new Coord(-1,-1),
		new Coord(-1, 0),
		new Coord( 0, 1),
		new Coord( 1, 1),
		new Coord( 1, 0),
		new Coord( 0,-1)
	};

	void MoveToRandomDirection()
	{
		// Check if hero near
		foreach (var neighbor in Map.neighbors)
		{
			if (Map.GetCell(coord + neighbor).unit is Hero)
			{
				if (enemyController == null)
				{
					return;
				}
				enemyController.Play("die");
				moveDirection = Vector3.zero;
				animationTimer = 50f;
				Timer.Add(0.3f, () => {
					if (Map.GetCell(coord + neighbor).baseUnit != null)
					{
						Map.GetCell(coord + neighbor).baseUnit.OnHit();
					}
					DieEffects();
				});
				return;
			}
		}
		// 5% wait chance
		if (Random.Range(0, 100) < 20)
		{
			needPoint = Map.CoordToHex(coord);
			moveDirection = Vector3.zero;
			animationTimer = moveTime * 0.5f;
			if (enemyController != null && isRun)
			{
				isRun = false;
				enemyController.SetBool("run", false);
				enemyController.Play("idle");
			}
			return;
		}
		// try 5 times to find random free place nearby
		for (int i = 0; i < 5; i++)
		{
			int direction = Random.Range(0, 6);
			if (Map.IsOnFloor(coord + directionShift[direction]) && !Map.IsUnitOnTile(coord + directionShift[direction]))
			{
				Map.MoveBaseUnit(this, coord, coord + directionShift[direction]);
				prevCoord = coord;
				coord = coord + directionShift[direction];
				needPoint = Map.CoordToHex(coord);
				moveDirection = transform.localPosition - needPoint;
				animationTimer = moveTime;
				if (enemyController != null && !isRun)
				{
					isRun = true;
					enemyController.SetBool("run", true);
					enemyController.Play("run");
				}
				needAngle = Utils.GetAngle(new Vector2(-moveDirection.x, -moveDirection.z));
				return;
			}
		}
		// wait if no free places around
		moveDirection = Vector3.zero;
		animationTimer = moveTime;
		if (enemyController != null && isRun)
		{
			isRun = false;
			enemyController.SetBool("run", false);
			enemyController.Play("idle");
		}
	}
	
	void Update()
	{
		if (Game.isPaused)
		{
			return;
		}
		animationTimer -= Time.deltaTime;
		transform.localPosition = needPoint + moveDirection * MoveFunction(animationTimer / moveTime);
		if (animationTimer <= 0)
		{
			MoveToRandomDirection();
		}
		if (angle != needAngle)
		{
			angle = Utils.RotateToAngle(angle, needAngle, rotationSpeed * Time.deltaTime);
			if (enemyController != null)
			{
				enemyController.transform.localEulerAngles = new Vector3(0, angle);
			}
		}
	}

	void DieEffects()
	{
		if (particles != null)
		{
			foreach(var emitter in particles)
			{
				if (emitter != null)
				{
					emitter.Play();
				}
			}
		}
		if (enemyController != null)
		{
			Destroy(enemyController.gameObject, 0.3f);
		}
		Destroy(gameObject, 2.2f);
		// sounds
		if (explosionSound != null && Settings.SoundEnabled)
		{
			explosionSound.Play();
		}
	}
	
	public override void OnHit()
	{
		Debug.Log("enemy hitted!");
		DieEffects();
	}
}
