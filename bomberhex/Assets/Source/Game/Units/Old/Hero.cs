using UnityEngine;
using System.Collections;

public class Hero : Unit
{
	[SerializeField]Animator heroController; // Set from Editor
	[SerializeField]GameObject shield = null; // Set from Editor
	Material shieldMaterial;

	public int Lives {
		get {
			return lives;
		}
		set {
			lives = Mathf.Min( maximumLives, value);
			GameUI.SetLifeCount(lives);
		}
	}
	
	const int maximumLives = 5;
	int lives = 3;
	
	float immortalityTimer = 0.01f;
	const float immortalTime = 2.0f;

	// for animation
	bool isRun = false;

	// for move
	const float moveTime = 0.3f;
//	float animationTimer = 0;
	Coord[] path = null;
	int nextPathSegment = 0;

	const float rotationSpeed = 360f * 2f; // degrees per second to rotate for hero

	public bool IsHeroMoving
	{
		get {return ActionTimer > 0.1f;}
	}

	public void MoveToPoint(Coord newCoord)
	{
		if (Map.IsOnFloor(newCoord))
		{
			path = Map.instance.FindPath(coord, newCoord);
			if (path != null)
			{
				nextPathSegment = path.Length - 2;
			}
		}
	}

	public void PlantBomb()
	{
		Bomb.Plant(transform.localPosition + new Vector3(0, 0.6f), coord, Player.info.bombPower);
	}

	void MoveToNextPathPoint()
	{
		MoveAnimated(path[nextPathSegment], moveTime);
		UpdateCurrentAction();
		nextPathSegment--;
		if (nextPathSegment < 0)
		{
			path = null;
		}

		needAngle = Utils.GetAngle(new Vector2(-moveVector.x, -moveVector.z));
	}

	void Start()
	{
		if (Map.instance == null)
		{
			return;
		}
		currentAngle = 180f;
     	needAngle = 180f;
		Vector3 needPoint = Map.CoordToHex(coord);
		transform.localPosition = needPoint;
		CameraController.StartAnimation(transform.localPosition);
		Timer.Add(1.2f, () =>
		{
			GameUI.SetLifeCount(lives);
		});
	}

	void CheckBonusCell()
	{
		if (Map.GetCell(coord).bonus != Bonus.Type.None)
		{
			if (Map.GetCell(coord).bonusObject != null)
			{
				Map.GetCell(coord).bonusObject.Catch();
				// aply bonus effects
			}
		}
	}

	void StartWinSequence()
	{
		Game.isPaused = true;
		// Victory!
		Map.instance.finalTeleport.AnimateTeleportation(() => {
			Game.isPaused = false;
			BaseUI.Show<WinnerMenuUI>(0);
		}, 0.1f);
		Timer.Add(-2.0f, 0.2f, (anim) =>
		{
			transform.localScale = Vector3.one * (1f - anim);
		});
	}

	void Update()
	{
		if (Game.isPaused)
		{
			return;
		}
		UpdateCurrentAction();
		CameraController.MoveTo(transform.localPosition);
		if (ActionTimer > 0)
		{
			if (!isRun)
			{
				isRun = true;
				heroController.SetBool("run", true);
			}
		}
		else
		{
			// check bonus
			CheckBonusCell();
			if (nextPathSegment >= 0 && path != null)
			{
				// move next
				MoveToNextPathPoint();
			}
			else
			{
				if (isRun)
				{
					isRun = false;
					heroController.SetBool("run", false);
				}
			}
			if (ActionTimer == 0)
			{
				if (coord == Map.instance.finalPoint)
				{
					StartWinSequence();
				}
			}
		}
		if (currentAngle != needAngle)
		{
			currentAngle = Utils.RotateToAngle(currentAngle, needAngle, rotationSpeed * Time.deltaTime);
			heroController.transform.localEulerAngles = new Vector3(0, currentAngle);
		}
		UpdateShield();
	}

	public override void OnHit()
	{
		if (immortalityTimer > 0)
		{
			return;
		}
		lives --;
		GameUI.SetLifeCount(lives);
		if (lives <= 0)
		{
			BaseUI.Show<LoserMenuUI>(0);
		}
		immortalityTimer = immortalTime;
		if (shield != null)
		{
			shield.SetActive(true);
		}
	}
	
	void UpdateShield()
	{ // Shield effect
		if (immortalityTimer > 0)
		{
			immortalityTimer -= Time.deltaTime;
			if (immortalityTimer <= 0)
			{
				immortalityTimer = 0;
				shield.SetActive(false);
			}
			// Shield animation
			if (shieldMaterial == null && shield != null && shield.GetComponent<Renderer>() != null)
			{
				shieldMaterial = shield.GetComponent<Renderer>().sharedMaterial;
			}
			if (shield != null && shieldMaterial != null)
			{
				float size = immortalityTimer / immortalTime * 10f;
				if (size >= 9f)
				{
					size = 10f - size;
					size = size * size;
				} else {
					if (size > 1f)
					{
						size = 1f;
					}
					else
					{
						size = size * size;
					}
				}
				shield.transform.localScale = Vector3.one * (size * 0.5f + 0.5f);
				shieldMaterial.color = new Color(1f, 1f, 1f, 0.7f * size);
			}
		}
	}
}
