﻿using UnityEngine;
using System.Collections;

public class BaseUnit : MonoBehaviour
{
	public Coord coord = Coord.Zero;
	public Coord prevCoord = Coord.Zero;
	
	protected float fullAnimationTime = 1f;
	protected float moveAnimationTimer = 0;
	protected Vector3 moveDirection = Vector3.zero;
	protected Vector3 needPoint = Vector3.zero;

	public static float MoveFunction(float anim) {
		return (anim * anim) * 0.4f + anim * 0.6f;
	}

	public bool IsMoving
	{
		get {
			return (moveAnimationTimer > 0);
		}
	}

	void Start()
	{
		transform.localPosition = Map.CoordToHex(coord);
	}

	public void MoveTo(Coord to, float animationTime)
	{

	}

	void UpdateAnimation()
	{
		if (Game.isPaused)
		{
			return;
		}
		if (moveAnimationTimer > 0)
		{
			moveAnimationTimer -= Time.deltaTime;
			if (moveAnimationTimer <= 0)
			{
				moveAnimationTimer = 0;

			}
		}
	}

	void Update()
	{
		
	}

	public virtual void OnHit()
	{
		Debug.Log("Im hitted!");
		Destroy(gameObject, 0.2f);
	}
}
