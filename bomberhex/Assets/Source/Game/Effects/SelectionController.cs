﻿using UnityEngine;
using System.Collections;

public class SelectionController : MonoBehaviour {

	static SelectionController instance = null;

	Transform selfTransform = null;
	bool needShow = false;
	float currentSize = 0;
	float currentWave = 0;

	void Start () {
		instance = this;
		selfTransform = transform;
	}

	public static void Set(Vector3 position, bool needShow = true)
	{
		if (instance != null)
		{
			instance.selfTransform.localPosition = position + new Vector3(0, 0.06f, 0);
			if (needShow)
			{
				instance.currentSize = 0;
			}
			instance.needShow = needShow;
			instance.SetSize();
		}
		else
		{
			Instantiate(Resources.Load("Prefabs/Effects/SelectionControllerMarker"));
		}
	}

	void SetSize()
	{
		selfTransform.localScale = Vector3.one * (currentSize + (Mathf.Sin(currentWave * 6.24f) * 0.2f * currentSize));
	}

	void Update () {
		if (needShow)
		{
			if (currentSize < 1f)
			{
				currentSize += Time.deltaTime * 2f;
				if (currentSize >= 1f)
				{
					currentSize = 1f;
				}
			}
			currentWave += Time.deltaTime;
			if (currentWave > 100f)
			{
				currentWave = 0;
			}
			SetSize();
		}
		else
		{
			if (currentSize > 0)
			{
				currentSize -= Time.deltaTime;
				if (currentSize <= 0f)
				{
					currentSize = 0f;
				}
				SetSize();
			}
		}
	}
}
