﻿using UnityEngine;
using System.Collections;

public class WallDestructionEffect : MonoBehaviour
{
	static WallDestructionEffect instance = null;

	[SerializeField]ParticleSystem particles = null; // Effect set from editor

	public static void EmitBricks(Vector3 position)
	{
		if (instance != null && instance.particles != null)
		{
			GameObject partObject = (GameObject)Instantiate(instance.particles.gameObject, position, Quaternion.Euler(270, 0, 0));
			ParticleSystem child = partObject.GetComponent<ParticleSystem>();
			child.Play();
			Destroy(partObject, 2f);
		}
		else
		{
			Instantiate(Resources.Load("Prefabs/Effects/WallDestructionEffect"));
		}
	}

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
	}
}
