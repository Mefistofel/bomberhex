﻿using UnityEngine;
using System.Collections;

public class InTeleportEffectController : MonoBehaviour
{

	[SerializeField]
	ParticleSystem[] teleportationEffects = null;

	[ContextMenu("Play Teleport Effect")]
	public void AnimateTeleportation()
	{
		PlayeEffects(teleportationEffects);
	}

	public void PlayeEffects (ParticleSystem[] effects)
	{
		if (effects != null)
		{
			foreach (var effect in effects)
			{
				if (effect != null)
				{
					effect.Play();
				}
			}
		}
	}
}
