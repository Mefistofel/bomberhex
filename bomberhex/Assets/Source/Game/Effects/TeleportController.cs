﻿using UnityEngine;
using System.Collections;

public class TeleportController : MonoBehaviour
{
	public bool isActive = false;
	public bool isFloating = false;

	public AnimationCurve floatingCurve = new AnimationCurve();
	
	[SerializeField]
	GameObject[] activeMarkers = null;
	[SerializeField]
	Transform[] rotatingParts = new Transform[5];
	[SerializeField]
	float[] maxFloatingHeight = new float[] { 0.1f, 0.2f, 0.3f, 0.4f, 0.5f };
	[SerializeField]
	float[] inFloatingHeight = new float[] { 0.02f, 0.02f, 0.04f, 0.4f, 0.4f };
	[SerializeField]
	float[] maxSpeedFactor = new float[] { 1f, 1f, 1.5f, 1.5f, 1.5f };
	[SerializeField]
	float rotationSpeed = 360f * 2f;
	[SerializeField]
	float activeSpeed = 120f;

	[SerializeField]
	ParticleSystem enableEffect = null;
	
	[SerializeField]
	ParticleSystem[] teleportationEffects = null;

	float[] rotationAngle = new float[5];
	float floatingAnimation = 0;
	float rotateAnimation = 0;

	void Start()
	{
		SetFloatingHeight();
	}

	[ContextMenu("Play Teleport Effect")]
	public void AnimateTeleportation(Action endAction = null, float timeBeforeStart = 1.2f)
	{
		Timer.Add(timeBeforeStart, () =>
		{
			isFloating = true;
			Timer.Add(1.2f, () =>
			{
				if (teleportationEffects != null)
				{
					foreach (var effect in teleportationEffects)
					{
						if (effect != null)
						{
							effect.Play();
						}
					}
				}
			});
			Timer.Add(2.8f, () =>
			{
				isFloating = false;
				if (endAction != null)
				{
					endAction();
				}
			});
		});
	}

	void SetFloatingHeight ()
	{
		for(int i = 0; i < rotatingParts.Length; i++)
		{
			if (rotatingParts[i] != null)
			{
				float animation = Mathf.Max(0f, Mathf.Min(1f, ((float)i * 0.08f - 0.32f + floatingAnimation * 1.32f)));
				animation = floatingCurve.Evaluate(animation);
				rotatingParts[i].localPosition = new Vector3(0, maxFloatingHeight[i] * animation + inFloatingHeight[i]);
			}
		}
	}

	void SetActiveMarkers(bool enable)
	{
		if (activeMarkers != null)
		{
			foreach(var marker in activeMarkers)
			{
				if (marker != null)
				{
					marker.SetActive(enable);
				}
			}
		}
	}

	void Update()
	{
		if (isActive)
		{
			if (rotateAnimation < 1f)
			{
				rotateAnimation += 1f * Time.deltaTime;
				rotateAnimation = Mathf.Min(1f, rotateAnimation);
				if (rotateAnimation >= 1f)
				{
					rotateAnimation = 1f;
					SetActiveMarkers(true);
					if (enableEffect != null)
					{
						enableEffect.Play();
					}
				}
			}
		}
		else
		{
			if (rotateAnimation > 0f)
			{
				rotateAnimation -= 0.8f * Time.deltaTime;
				rotateAnimation = Mathf.Max(0f, rotateAnimation);
				if (rotateAnimation <= 0f)
				{
					rotateAnimation = 0f;
					SetActiveMarkers(false);
					if (enableEffect != null)
					{
						enableEffect.Stop();
					}
				}
			}
		}
		if (rotateAnimation > 0) // rotation
		{
			float slowSpeed = activeSpeed * (1f -  floatingAnimation * floatingAnimation) * rotateAnimation;
			float workSpeed = rotationSpeed * floatingAnimation * floatingAnimation;
			for(int i = 0; i < rotatingParts.Length; i++)
			{
				rotationAngle[i] += (slowSpeed + workSpeed) * maxSpeedFactor[i] * Time.deltaTime;
				if (rotationAngle[i] > 360f)
				{
					rotationAngle[i] -= 360f;
				}
				if (rotatingParts[i] != null)
				{
					rotatingParts[i].localEulerAngles = new Vector3(-90, 0, rotationAngle[i]);
				}
			}
		}

		if (isFloating)
		{
			if (floatingAnimation < 1f)
			{
				floatingAnimation += 1.4f * Time.deltaTime;
				floatingAnimation = Mathf.Min(1f, floatingAnimation);
				SetFloatingHeight();
			}
		}
		else
		{
			if (floatingAnimation > 0f)
			{
				floatingAnimation -= 0.8f * Time.deltaTime;
				floatingAnimation = Mathf.Max(0f, floatingAnimation);
				SetFloatingHeight();
			}
		}
	}
}
