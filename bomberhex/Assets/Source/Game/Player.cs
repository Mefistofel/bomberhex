﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

[System.Serializable]
public class Player
{
	public static Player info;
	const string defaultPlayerFileName = "player.xml";
	// increase by bonuses
	[XmlAttribute ("bomb_power")]
	public int bombPower = 1;
	[XmlAttribute ("bomb_count")]
	public int bombCount = 1;
	[XmlAttribute ("current_level")]
	public int level = 0;

	static Player()
	{
		Load();
	}
	
	public Player()
	{
	}

	public static void Load(string fileName = defaultPlayerFileName)
	{
		if (File.Exists(Application.persistentDataPath + "/" + fileName))
		{
			try
			{
				XmlSerializer serialReader = new XmlSerializer(typeof(Player));
				Stream reader = new FileStream(Application.persistentDataPath + "/" + fileName, FileMode.Open);
				info = serialReader.Deserialize(reader) as Player;
				reader.Close();
			} catch
			{
				info = new Player();
			}
		}
		else
		{
			info = new Player();
		}
	}

	public static void Save(string fileName = defaultPlayerFileName)
	{
		XmlSerializer serialWrite = new XmlSerializer(typeof(Player));
		XmlTextWriter writer = new XmlTextWriter(Application.persistentDataPath + "/" + fileName, System.Text.UTF8Encoding.UTF8);
		serialWrite.Serialize(writer, info);
		writer.Close();
	}
}
