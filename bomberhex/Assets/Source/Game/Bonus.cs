﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

public class Bonus : MonoBehaviour
{
	[System.Serializable]
	public enum Type
	{
		[XmlEnum("")]
		None = 0,
		[XmlEnum("h")]
		Heal = 1,
		[XmlEnum("p")]
		Power = 2,
		[XmlEnum("b")]
		Bomb = 3
	}

	Transform selfTransform = null;
	[SerializeField]AnimationCurve curve = null;
	[SerializeField]Type type = Type.None;
	[SerializeField]ParticleSystem catchEffect = null; // Set from editor
	[SerializeField]AudioSource soundEffect = null; // Set from editor

	public bool alreadyCatched = false;
	
	const float rotationTime = 3.2f;
	float timer = 0;
	float angle = 0;

	public void Catch()
	{
		if (alreadyCatched)
		{
			return;
		}
		ApplyEffect();
		alreadyCatched = true;
		if (catchEffect != null)
		{
			catchEffect.Play();
		}
		if (soundEffect != null && Settings.SoundEnabled)
		{
			soundEffect.Play();
		}
		Timer.Add(0.4f, (anim) =>
		{
			angle += anim * 0.3f;
			selfTransform.eulerAngles = new Vector3(0, angle * 360f);
			selfTransform.localScale = Vector3.one * (1f - anim * anim);
		});
		Destroy(gameObject, 3f);
	}

	void ApplyEffect()
	{
		switch (type)
		{
			case Type.Heal:
				Game.currentHero.Lives += 1;
				break;
			case Type.Bomb:
				Player.info.bombCount += 1;
				Player.Save();
				break;
			case Type.Power:
				Player.info.bombPower += 1;
				break;
		}
	}

	void Start()
	{
		selfTransform = transform;
	}

	void Update()
	{
		timer -= Time.deltaTime;
		if (timer < 0)
		{
			timer = rotationTime;
		}
		angle = timer / rotationTime;
		if (curve != null)
		{
			angle = curve.Evaluate(angle);
		}
		selfTransform.eulerAngles = new Vector3(0, angle * 360f);
	}
}
