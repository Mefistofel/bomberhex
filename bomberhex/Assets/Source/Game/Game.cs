using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class Game : MonoBehaviour
{
	static Game instance;
	public static bool isPaused = false;

	public static Hero currentHero
	{
		get
		{
			if (instance == null)
				return null;
			return instance.hero;
		}
	}

	[SerializeField]
	public Camera controlCamera = null; // Set from editor
	[SerializeField]
	public PhysicsRaycaster raycaster = null; // Set from editor
	[SerializeField]
	public Material mapMaterial = null; // Set from editor
	[SerializeField]
	public TextAsset levelAsset = null; // Set from editor
	
	public static int currentBombCount = 0;
	Map map = null;
	Hero hero = null;
	Enemy[] enemy = null;

	public static void Restart()
	{
		// killall
		if (instance != null)
		{
			if (instance.map != null)
			{
				Destroy(instance.map.gameObject);
			}
			if (instance.hero != null)
			{
				Destroy(instance.hero.gameObject);
			}
			if (instance.enemy != null)
			{
				foreach (var enemy in instance.enemy)
				{
					if (enemy != null)
					{
						Destroy(enemy.gameObject);
					}
				}
				instance.enemy = null;
			}
			instance.Start();
		}
	}

	void Start()
	{
		isPaused = false;
		instance = this;
		if (levelAsset != null)
		{
			map = LevelGenerator.LoadLevel(transform, levelAsset);
		}
		else
		{
			map = LevelGenerator.LoadLevel(transform, 2);// Player.info.level);
			// map = LevelGenerator.LoadLevel(transform, Player.info.level);
		}
//		map.gameObject.AddComponent<MeshCombiner>().material = mapMaterial;
		// enemies
		enemy = map.GetMapEnemies();
		hero = Utils.LoadPrefab<Hero>("Prefabs/Units/Hero", transform);
		hero.coord = map.GetHeroStartPoint();
		AnimateHeroTeleportation();
		currentBombCount = 0;

//		EnemyElectro unit = Utils.LoadPrefab<EnemyElectro>("Prefabs/Units/Enemy_Electro", transform);
//		unit.coord = new Coord(6, 7);
//		EnemyElectro unit1 = Utils.LoadPrefab<EnemyElectro>("Prefabs/Units/Enemy_2", transform);
		//		unit1.coord = new Coord(7, 7);
		for (int i = 0; i < 13; i++)
		{
			DrillingBot unit = Utils.LoadPrefab<DrillingBot>("Prefabs/Units/DrillerBot", transform);
			unit.coord = new Coord(6 + i, 7);
		}

//		for(int i = 0; i < 10; i++)
//		{
//			EnemyElectro unit = Utils.LoadPrefab<EnemyElectro>("Prefabs/Units/Enemy_Electro", transform);
//			unit.coord = new Coord(Random.Range(1,12), Random.Range(1,12));
//		}
	}

	void AnimateHeroTeleportation()
	{
		if (hero != null)
		{
			isPaused = true;
			Transform heroTransform = hero.transform;
			heroTransform.localScale = Vector3.zero;
			Timer.Add(-2f, 0.2f, (anim) =>
			{
				heroTransform.localScale = Vector3.one * anim;
			},
			() =>
			{
				isPaused = false;
				heroTransform.localScale = Vector3.one;
			});
		}
		Timer.Add(1f, () =>
		{
			GameObject startCellObject = Map.GetCell(map.GetHeroStartPoint()).structureObject;
			if (startCellObject != null)
			{
				InTeleportEffectController controller = startCellObject.GetComponent<InTeleportEffectController>();
				if (controller != null)
				{
					controller.AnimateTeleportation();
				}
			}
		});
	}

	Coord GetRandomCoord()
	{
		Coord coord = new Coord();
		for (int i = 0; i < 100; i++)
		{
			coord = new Coord(Random.Range(0, map.width), Random.Range(0, map.height));
			if (Map.IsOnFloor(coord))
			{
				return coord;
			}
		}
		return coord;
	}

	Vector3 RaycastFloor(Vector2 screenPoint)
	{
		Vector3 point = Vector3.zero;
		if (controlCamera != null)
		{
			var ray = controlCamera.ScreenPointToRay(screenPoint);
			Plane floorPlane = new Plane(Vector3.up, Vector3.zero);
			float distance = 0; 
			if (floorPlane.Raycast(ray, out distance))
			{
				return ray.GetPoint(distance);
			}
		}
		return point;
	}

	public static void PlantBomb()
	{
		if (instance != null && instance.hero != null)
		{
			if (Game.currentBombCount >= Player.info.bombCount)
			{
				return;
			}
			Game.currentBombCount++;
			instance.hero.PlantBomb();
		}
	}
}