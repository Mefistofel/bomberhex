using UnityEngine;
using System.Collections;

public class LevelGenerator
{
	
	public static Map LoadLevel(Transform parent, TextAsset dataAsset)
	{
		MapTileset source = MapTileset.LoadFromAsset(dataAsset);
		Map map = Utils.Create<Map> (parent);
		map.width = source.width;
		map.height = source.height;
		map.cell = new Map.Cell[source.width, source.height];
		for (int i = 0; i < map.width; i++)
		{
			for (int j = 0; j < map.height; j++)
			{
				map.cell[i, j] = new Map.Cell() {
					type = source[i, j].type,
					structure = source[i, j].structure,
					angle = source[i, j].angle,
					bonus = source[i, j].bonus
				};
				if (source[i, j].enemy != EnemyType.None)
				{
					AddEnemy(source[i, j].enemy, parent, map, new Coord(i, j)); 
				}
				if (source[i, j].structure == CellStructure.StartTeleport)
				{
					map.startPoint = new Coord(i, j);
				}
				if (source[i, j].structure == CellStructure.FinalTeleport)
				{
					map.finalPoint = new Coord(i, j);
				}
			}
		}
		return map;
	}

	public static Map LoadLevel(Transform parent, int levelNum = 0)
	{
		Map map = Utils.Create<Map> (parent);
		int width = 23;
		int height = 23;
		switch (levelNum)
		{
			case -1:
				width = 15;
				height = 15;
				GenerateBase(map, width, height);
				GenerateTrianglePattern(0, map, CellType.Armor, 0.2f);
				GenerateTrianglePattern(1, map, CellType.None, 0.4f);
				GenerateTrianglePattern(2, map, CellType.Wall, 0.5f);
//				GenerateTrianglePattern(2, map, CellType.Wall, 0.3f);
//				AddEnemy(parent, map, new Coord(8,6));
//				AddEnemy(parent, map, new Coord(9,8));
//				AddRandomWallBonus(map, Bonus.Type.Power);
//				AddRandomWallBonus(map, Bonus.Type.Bomb);
				map.cell[2, 1].type = CellType.Floor;
				map.cell[1, 2].type = CellType.Floor;
				map.startPoint = new Coord(1, 1);
				map.finalPoint = new Coord(9, 9);
//				map.cell[1, 1].type = CellType.StartPoint;// Add start point
//				map.cell[9, 9].type = CellType.FinalWall;// Add end point
				break;
			case 0:
				width = 11;
				height = 11;
				GenerateBase(map, width, height);
				GenerateTrianglePattern(0, map, CellType.Armor, 0.8f);
				GenerateTrianglePattern(1, map, CellType.Wall, 0.5f);
				GenerateTrianglePattern(2, map, CellType.Wall, 0.3f);
				AddEnemy(parent, map, new Coord(8,6));
				AddEnemy(parent, map, new Coord(9,8));
				AddRandomWallBonus(map, Bonus.Type.Power);
				AddRandomWallBonus(map, Bonus.Type.Bomb);
				map.cell[2, 1].type = CellType.Floor;
				map.cell[1, 2].type = CellType.Floor;
				map.startPoint = new Coord(1, 1);
				map.finalPoint = new Coord(9, 9);
//				map.cell[1, 1].type = CellType.StartPoint;// Add start point
//				map.cell[9, 9].type = CellType.FinalWall;// Add end point
				break;
			case 1:
				width = 13;
				height = 13;
				GenerateBase(map, width, height);
				GenerateTrianglePattern(0, map, CellType.Armor, 0.8f);
				GenerateTrianglePattern(1, map, CellType.Wall, 0.5f);
				GenerateTrianglePattern(2, map, CellType.Wall, 0.3f);
				AddEnemy(parent, map, new Coord(8,6));
				AddEnemy(parent, map, new Coord(8,8));
				AddEnemy(parent, map, new Coord(9,8));
				AddRandomWallBonus(map, Bonus.Type.Bomb);
				AddRandomWallBonus(map, Bonus.Type.Power);
				map.cell[2, 1].type = CellType.Floor;
				map.cell[1, 2].type = CellType.Floor;
				map.startPoint = new Coord(2, 2);
				map.finalPoint = new Coord(6, 10);
//				map.cell[2, 2].type = CellType.StartPoint;// Add start point
//				map.cell[6, 10].type = CellType.FinalWall;// Add end point
				break;
			case 2:
				width = 16;
				height = 16;
				GenerateBase(map, width, height);
				GenerateTrianglePattern(2, map, CellType.Armor, 0.5f);
				GenerateTrianglePattern(2, map, CellType.Wall, 0.4f);
				GenerateTrianglePattern(1, map, CellType.TreeWall, 0.3f);
				GenerateTrianglePattern(0, map, CellType.TreeWall, 0.5f);
				GenerateTrianglePattern(1, map, CellType.None, 0.4f);
				AddEnemy(parent, map, new Coord(8,6));
				AddEnemy(parent, map, new Coord(9,8));
				AddEnemy(parent, map, new Coord(6,4));
				AddEnemy(parent, map, new Coord(6,5));
				AddRandomWallBonus(map, Bonus.Type.Power);
				AddRandomWallBonus(map, Bonus.Type.Heal);
				map.cell[2, 1].type = CellType.Floor;
				map.cell[1, 2].type = CellType.Floor;
				map.startPoint = new Coord(2, 2);
				map.finalPoint = new Coord(12, 4);
//				map.cell[2, 2].type = CellType.StartPoint;// Add start point
//				map.cell[12, 4].type = CellType.FinalWall;// Add end point
				break;
			case 3:
				width = 18;
				height = 18;
				GenerateBase(map, width, height);
				GenerateTrianglePattern(3, map, CellType.TreeWall, 0.1f);
				GenerateTrianglePattern(3, map, CellType.Armor, 0.7f);
				GenerateTrianglePattern(4, map, CellType.Wall, 0.5f);
				GenerateTrianglePattern(5, map, CellType.Wall, 0.3f);
				GenerateTrianglePattern(4, map, CellType.TreeWall, 0.2f);
				GenerateTrianglePattern(5, map, CellType.TreeWall, 0.1f);
				AddEnemy(parent, map, new Coord(8,6));
				AddEnemy(parent, map, new Coord(9,8));
				AddEnemy(parent, map, new Coord(6,4));
				AddEnemy(parent, map, new Coord(5,5));
				AddEnemy(parent, map, new Coord(6,5));
				AddRandomWallBonus(map, Bonus.Type.Heal);
				AddRandomWallBonus(map, Bonus.Type.Bomb);
				map.cell[2, 1].type = CellType.Floor;
				map.cell[1, 2].type = CellType.Floor;
				map.cell[1, 2].type = CellType.Floor;
				map.startPoint = new Coord(2, 2);
				map.finalPoint = new Coord(16, 14);
//				map.cell[2, 2].type = CellType.StartPoint;// Add start point
//				map.cell[16, 14].type = CellType.FinalWall;// Add end point
				break;
			case 4:
			default:
				width = 23;
				height = 23;
				GenerateBase(map, width, height);
				Coord randomFinalPoint = FindFreeCell(map);
				GenerateTrianglePattern(Random.Range(0, 6), map, CellType.TreeWall, Random.Range(0, 1f));
				GenerateTrianglePattern(Random.Range(0, 6), map, CellType.Wall, Random.Range(0, 1f));
				GenerateTrianglePattern(Random.Range(0, 6), map, CellType.Armor, Random.Range(0, 1f));
				GenerateTrianglePattern(Random.Range(0, 6), map, CellType.TreeWall, Random.Range(0, 1f));
				GenerateTrianglePattern(Random.Range(0, 6), map, CellType.Wall, Random.Range(0, 1f));
				GenerateTrianglePattern(Random.Range(0, 6), map, CellType.Armor, Random.Range(0, 1f));
				for (int i = 0; i < levelNum + 1; i++)
				{
					Coord coord = FindFreeCell(map);
					AddEnemy(parent, map, coord);
				}
				AddRandomWallBonus(map, Bonus.Type.Heal);
				if (levelNum < 6)
				{
					if (Player.info.bombCount < 3)
					{
						AddRandomWallBonus(map, Bonus.Type.Bomb);
					}
					if (Player.info.bombPower < 3)
					{
						AddRandomWallBonus(map, Bonus.Type.Power);
					}
				} else if (levelNum < 8)
				{
					if (Player.info.bombCount < 4)
					{
						AddRandomWallBonus(map, Bonus.Type.Bomb);
					}
					if (Player.info.bombPower < 4)
					{
						AddRandomWallBonus(map, Bonus.Type.Power);
					}
				} else if (levelNum < 12)
				{
					AddRandomWallBonus(map, Bonus.Type.Heal);
					if (Player.info.bombCount < 5)
					{
						AddRandomWallBonus(map, Bonus.Type.Bomb);
					}
					if (Player.info.bombPower < 5)
					{
						AddRandomWallBonus(map, Bonus.Type.Power);
					}
				}
				map.cell[2, 1].type = CellType.Floor;
				map.cell[1, 2].type = CellType.Floor;
				map.startPoint = new Coord(2, 2);
				map.finalPoint = new Coord(randomFinalPoint.x, randomFinalPoint.y);
//				map.cell[2, 2].type = CellType.StartPoint;// Add start point
//				map.cell[randomFinalPoint.x, randomFinalPoint.y].type = CellType.FinalWall;// Add end point
				break;
		}
		Map.shift = new Vector3(-(float)(map.width - 1f) * 0.5f * 0.866f, 0, -(float)(map.height - 1f) * 0.5f + (float)(map.width - 1) * 0.25f) * Map.Cell.size;
		return map;
	}

	static Coord FindFreeCell(Map map)
	{
		Coord randomCoord = new Coord(Random.Range(0, map.width - 1), Random.Range(0, map.height - 1));
		for(int i = 0; i < 200; i ++)
		{
			if (map.cell[randomCoord.x, randomCoord.y].type == CellType.Floor && !(randomCoord.x < 4 & randomCoord.y < 4))
			{
				return randomCoord;
			}
			randomCoord = new Coord(Random.Range(0, map.width - 1), Random.Range(0, map.height - 1));
		}
		return randomCoord;
	}

	static void AddRandomWallBonus(Map map, Bonus.Type bonusType)
	{
		Coord randomCoord = new Coord(Random.Range(0, map.width - 1), Random.Range(0, map.height - 1));
		for(int i = 0; i < 200; i ++)
		{
			if (map.cell[randomCoord.x, randomCoord.y].type == CellType.Wall || map.cell[randomCoord.x, randomCoord.y].type == CellType.TreeWall)
			{
				map.cell[randomCoord.x, randomCoord.y].bonus = bonusType;
				return;
			}
			randomCoord = new Coord(Random.Range(0, map.width - 1), Random.Range(0, map.height - 1));
		}
	}
	static Enemy AddEnemy(EnemyType type, Transform parent, Map map, Coord coord)
	{
		Enemy enemy = Utils.LoadPrefab<Enemy>("Prefabs/Units/" + type.ToString(), parent);
		if (enemy != null)
		{
			map.cell[coord.x, coord.y].baseUnit = enemy;
			map.cell[coord.x, coord.y].type = CellType.Floor;
			enemy.coord = coord;
		}
		return enemy;
	}

	static Enemy AddEnemy(Transform parent, Map map, Coord coord)
	{
		Enemy enemy = Utils.LoadPrefab<Enemy>("Prefabs/Units/Enemy_1", parent);
		map.cell[coord.x, coord.y].baseUnit = enemy;
		map.cell[coord.x, coord.y].type = CellType.Floor;
		enemy.coord = coord;
		return enemy;
	}

	static void GenerateBase(Map map, int width = 23, int height = 23)
	{
		map.width = width;
		map.height = height;
		map.cell = new Map.Cell[width, height];
		// Create flat hex
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				map.cell[i, j] = new Map.Cell();
				map.cell[i, j].type = CellType.Floor;
				if (j + (width * 0.5f - 1) - i < 0)
				{
					map.cell[i, j].type = CellType.None;
				}
				if (j - height + (width * 0.5f + 1) - i > 0)
				{
					map.cell[i, j].type = CellType.None;
				}
			}
		}
		// borders
		for (int i = 0; i < width; i++)
		{
			map.cell[i, 0].type = CellType.None;
			map.cell[i, (height - 1)].type = CellType.None;
		}
		for (int j = 0; j < height; j++)
		{
			map.cell[0, j].type = CellType.None;
			map.cell[width - 1, j].type = CellType.None;
		}
	}

	public static void GenerateTrianglePattern(int pattern, Map map, CellType type = CellType.Armor, float chance = 1.0f)
	{
		for (int i = 0; i < map.width; i++)
		{
			for (int j = 0; j < map.height; j++)
			{
				if (map.cell[i, j].type == CellType.Floor)
				{
					bool patternParam = (j % 3 == 1 && i % 3 == 1 ||
					                     j % 3 == 0 && i % 3 == 2 ||
					                     j % 3 == 2 && i % 3 == 0);
					if (pattern == 1)
					{
						patternParam = (j % 3 == 0 && i % 3 == 1 ||
						                j % 3 == 2 && i % 3 == 2 ||
						                j % 3 == 1 && i % 3 == 0);
					}
					if (pattern == 2)
					{
						patternParam = (j % 3 == 2 && i % 3 == 1 ||
						                j % 3 == 1 && i % 3 == 2 ||
						                j % 3 == 0 && i % 3 == 0);
					}
					if (pattern == 3 || pattern == 4  || pattern == 5)
					{
						patternParam = (j % 3 == pattern - 3);
					}

					if (patternParam)
					{
						if (Random.Range(0, 100) < chance * 100f)
						{
							map.cell[i, j].type = type;
						}
					}
				}
			}
		}
	}
//		< map.width; i++)
//		{
//			for (int j = 0; j < map.height; j++)
//			{
//				if (map.cell[i, j].type == CellType.Floor)
//				{
//					if (j % 3 == 1 && i % 3 == 1 ||
//					    j % 3 == 0 && i % 3 == 2 ||
//					    j % 3 == 2 && i % 3 == 0)
//					{
//						if (Random.Range(0, 100) < 30)
//						{
//							map.cell[i, j].type = CellType.Armor;
//						}
//					}
//				}
//			}
//		}
		//				if (map.cell[i, j].type == CellType.Floor)
//				{
//					if (j % 3 == 1 && i % 3 == 0 ||
//					    j % 3 == 0 && i % 3 == 1 ||
//					    j % 3 == 2 && i % 3 == 2)
//					{
//						if (Random.Range(0, 100) < 40)
//						{
//							mapCell[i, j].type = CellType.Wall;
//						}
//					}
//				}
//			}
//		}
		//Bonus
//		map.cell[4, 4].type = CellType.Wall;
		//		map.cell[4, 5].type = CellType.Wall;
		//		map.cell[4, 6].type = CellType.Wall;
		//		map.cell[4, 4].bonus = Bonus.Type.Power;
		//		map.cell[4, 5].bonus = Bonus.Type.Heal;
		//		map.cell[4, 6].bonus = Bonus.Type.Bomb;
}
