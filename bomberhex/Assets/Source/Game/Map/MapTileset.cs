﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public enum CellType
{
	[XmlEnum(" ")] None,
	[XmlEnum("f")] Floor,
	[XmlEnum("w")] Wall, // can destroy, exposion doesn't run after it
	[XmlEnum("t")] TreeWall, // can destroy, exposion run after it
	[XmlEnum("a")] Armor // can't destroy
}

[Serializable]
public enum CellStructure
{
	[XmlEnum(" ")]  None,
	[XmlEnum("fl")] Floor,
	[XmlEnum("wl")] WallLine,
	[XmlEnum("wt")] WallTile,
	[XmlEnum("al")] ArmorLine,
    [XmlEnum("at")] ArmorTile,
    [XmlEnum("tt")] TreeTile,
    [XmlEnum("tl")] TreeLine,
    [XmlEnum("st")] StartTeleport,
	[XmlEnum("ft")] FinalTeleport,
	[XmlEnum("tu")] TubeTile,
	[XmlEnum("ta")] TubeAngularTile,
	[XmlEnum("tc")] TubeCrushedTile,
	[XmlEnum("tb")] TubeGroundTile,
	[XmlEnum("tg")] TubeInGroundTile,
	[XmlEnum("tr")] TubeTriangleTile,
	[XmlEnum("tv")] TubeValveTile,
	[XmlEnum("tung")] TubeTileNoGround,
	[XmlEnum("tang")] TubeAngularTileNoGround,
	[XmlEnum("tcng")] TubeCrushedTileNoGround,
	[XmlEnum("trng")] TubeTriangleTileNoGround,
	[XmlEnum("tvng")] TubeValveTileNoGround,
	[XmlEnum("ttb")] TubeTopTile,
	[XmlEnum("ttg")] TubeTopStandTile,
	[XmlEnum("ttr")] TubeUpTile,
	[XmlEnum("ttv")] TubeTriTile,
}

[Serializable]
public enum EnemyType
{
	[XmlEnum(" ")]  None,
	[XmlEnum("ft")] Enemy_1,
	[XmlEnum("fl")] Enemy_Driller,
	[XmlEnum("eg")] Enemy_Minigun,
	[XmlEnum("er")] Enemy_Rocket,
}

[Serializable]
public struct Cell {
	[XmlAttribute("a")]
	public int angle;

	[XmlAttribute("t")]
	public CellType type;
	
	[XmlAttribute("s")]
	public CellStructure structure;

	[XmlAttribute("e")]
	public EnemyType enemy;
	
	[XmlAttribute("b")]
	public Bonus.Type bonus;
}

// class for serialization, storing and editing of map structure
[Serializable]
public class MapTileset
{
	[XmlAttribute("level_name")]
	public string name;
	[XmlAttribute("comment")]
	public string comment;

	[XmlAttribute("w")]
	public int width = 10;

	[XmlAttribute("h")]
	public int height = 10;

	[XmlArray("cells")]
	[XmlArrayItem("cell")]
	public Cell[] cells = new Cell[10 * 10];
	
	[XmlAttribute("sx")]
	float shiftX = 0;
	[XmlAttribute("sy")]
	float shiftY = 0;
	[XmlAttribute("sz")]
	float shiftZ = 0;

	public Vector3 Shift {
		get
		{ return new Vector3(shiftX, shiftY, shiftZ);}
		set
		{
			shiftX = value.x;
			shiftY = value.y;
			shiftZ = value.z;
		}
	}

	public Cell this[int x, int y] {
		get {
			if (cells != null && x >= 0 && y >= 0 && x < width && y < height && x * width + y < cells.Length) {
				return cells[x * width + y];
			}
			return new Cell(){type = CellType.None};
		}
		set {
			if (cells != null && x >= 0 && y >= 0 && x < width && y < height && x * width + y < cells.Length) {
				cells[x * width + y] = value;
			}
		}
	}

	public Cell this[Coord coord] {
		get {
			return this[coord.x, coord.y];
		}
		set {
			this[coord.x, coord.y] = value;
		}
	}

	public static CellType GetType(CellStructure structure)
	{
		switch (structure)
		{
			default:
			case CellStructure.None:
			case CellStructure.TubeTileNoGround:
			case CellStructure.TubeAngularTileNoGround:
			case CellStructure.TubeCrushedTileNoGround:
			case CellStructure.TubeTriangleTileNoGround:
			case CellStructure.TubeValveTileNoGround:
				return CellType.None;
			case CellStructure.ArmorLine:
			case CellStructure.ArmorTile:
			case CellStructure.TubeTile:
			case CellStructure.TubeAngularTile:
			case CellStructure.TubeCrushedTile:
			case CellStructure.TubeGroundTile:
			case CellStructure.TubeInGroundTile:
			case CellStructure.TubeTriangleTile:
			case CellStructure.TubeValveTile:
			case CellStructure.TubeTopStandTile:
			case CellStructure.TubeUpTile:
			case CellStructure.TubeTriTile:
				return CellType.Armor;
			case CellStructure.StartTeleport:
			case CellStructure.FinalTeleport:
			case CellStructure.Floor:
			case CellStructure.TubeTopTile:
				return CellType.Floor;
			case CellStructure.WallLine:
			case CellStructure.WallTile:
				return CellType.Wall;
			case CellStructure.TreeLine:
			case CellStructure.TreeTile:
				return CellType.TreeWall;
		}
	}
	
	public MapTileset(){}

	public MapTileset(int mapWidth = 10, int mapHeight = 10)
	{
		width = mapWidth;
		height = mapHeight;
		cells = new Cell[width * height];
		for (int i = 0; i < cells.Length; i++)
		{
			cells[i] = new Cell();
		}
	}
	
	public static MapTileset LoadFromAsset(TextAsset dataAsset) {
		if (dataAsset != null){
			XmlSerializer serialReader = new XmlSerializer(typeof(MapTileset));
			Stream writer = new MemoryStream(dataAsset.bytes);
			MapTileset mapInfo = (MapTileset)serialReader.Deserialize(writer);
			writer.Close();
			return mapInfo;
		} else {
			Debug.Log("dataAsset not setted");
		}
		return null;
	}

	#if UNITY_EDITOR
	// Save file in resources only from editor
	public static void Save (MapTileset structure, string fileName) {
		XmlSerializer serialWrite = new XmlSerializer(typeof(MapTileset));
		XmlTextWriter writer = new XmlTextWriter (fileName, System.Text.UTF8Encoding.UTF8);
		writer.Formatting = Formatting.Indented;
		serialWrite.Serialize (writer, structure);
		writer.Close();
	}
	public void SaveToAsset(TextAsset dataAsset) {
		Save (this, AssetDatabase.GetAssetPath(dataAsset));
		Debug.Log("Save map to " + AssetDatabase.GetAssetPath(dataAsset));
	}
	#endif

}
