using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Map : MonoBehaviour
{
	public static Map instance = null; // Can be only one per scene
	
	public static Vector3 shift = Vector3.zero;
	
	public static readonly Coord[] neighbors = new Coord[]
	{
		new Coord( 0, 1),
		new Coord( 1, 1),
		new Coord( 1, 0),
		new Coord( 0,-1),
		new Coord(-1,-1),
		new Coord(-1, 0)
	};
	public static readonly float[] neighborAngle = {
		0,
		60,
		120,
		180,
		240,
		300
	};

	public struct Cell
	{
		public const float size = 1.0f;
		public CellType type;
		public CellStructure structure;
		public GameObject structureObject;
		public Unit unit;
		public Unit prevUnit;
		public BaseUnit baseUnit;
		public Bomb bomb;
		public int angle;
		public Bonus.Type bonus;
		public Bonus bonusObject;
		public int marker; // for path find
		public void OnHit(Coord hitPosition)
		{
			if (unit != null)
			{
				unit.TryHit(hitPosition);
			}
			if (prevUnit != null)
			{
				prevUnit.TryHit(hitPosition);
			}
			// TODO kill baseUnit
			if (baseUnit != null)
			{
				baseUnit.OnHit();
			}
		}
		public void CreateCell(CellType newType, int x, int y)
		{
			type = newType;
			if (structureObject != null)
			{
				Destroy(structureObject);
			}
			if (type != CellType.None)
			{	// Create floor
				if (structure == CellStructure.Floor)
				{	// Create random floor on clear places
					Map.instance.LoadPrefab(x, y, 60f * Random.Range(0, 6), "Floor/FloorTile_" + Random.Range(0, 9));
				}
				else
				{	// Clear under some things
					Map.instance.LoadPrefab(x, y, 60f * Random.Range(0, 6), "Floor/FloorTile_" + Random.Range(0, 1));
				}
			}
			if (structure != CellStructure.None && structure != CellStructure.Floor)
			{
				structureObject = Map.instance.LoadPrefab(x, y, angle, "Enviroment/" + structure.ToString());
			}
			if (bonus != Bonus.Type.None && type != CellType.Wall && type != CellType.TreeWall)
			{
				Debug.Log("Bonus!");
				bonusObject = Map.instance.LoadPrefab(x, y, angle, "Bonus/Bonus" + bonus.ToString()).GetComponent<Bonus>();
			}
		}
	}

	public int width = 20;
	public int height = 20;

	public Cell[,] cell = null;
	public TeleportController finalTeleport = null;
	public Coord startPoint = new Coord();
	public Coord finalPoint = new Coord();

	public static Vector3 CoordToHex(int x, int y)
	{
		return new Vector3(x * 0.866f, 0, y - x * 0.5f) * Cell.size + shift;
	}
	
	public static Vector3 CoordToHex(Coord coord)
	{
		return CoordToHex(coord.x, coord.y);
	}

	// path find
	public static Coord HexToCoord(Vector3 hexPoint)
	{
		hexPoint -= shift;
		hexPoint /= Cell.size;
		hexPoint.x /= 0.866f;
		return new Coord((int)Mathf.Round(hexPoint.x), (int)Mathf.Round(hexPoint.z + hexPoint.x * 0.5f));
	}

	public static void AddBomd(Coord coord, Bomb bomb)
	{
		if (instance == null || coord.x < 0 || coord.y < 0 || coord.x >= instance.width || coord.y >= instance.height)
		{
			return;
		}
		instance.cell[coord.x, coord.y].bomb = bomb;
	}

	public static Cell GetCell(Coord coord)
	{
		if (instance == null || coord.x < 0 || coord.y < 0 || coord.x >= instance.width || coord.y >= instance.height)
		{
			return new Cell() {type = CellType.None};
		}
		return instance.cell[coord.x, coord.y];
	}

	public static bool IsOnFloor(Coord coord)
	{
		if (instance == null || coord.x < 0 || coord.y < 0 || coord.x >= instance.width || coord.y >= instance.height)
		{
			return false;
		}
		CellType type = instance.cell[coord.x, coord.y].type; 
		return (type == CellType.Floor);
	}

	public static bool IsUnitOnTile(Coord coord)
	{
		if (instance == null || coord.x < 0 || coord.y < 0 || coord.x >= instance.width || coord.y >= instance.height)
		{
			return false;
		}
		return instance.cell[coord.x, coord.y].unit != null;
	}

	public static void MoveBaseUnit(BaseUnit unit, Coord from, Coord to)
	{
		if (instance != null)
		{
			if (from.x >= 0 && from.y >= 0 && from.x < instance.width && from.y < instance.height &&
				to.x >= 0 && to.y >= 0 && to.x < instance.width && to.y < instance.height)
			{
				if (Map.instance.cell[from.x, from.y].baseUnit == unit)
				{
					Map.instance.cell[from.x, from.y].baseUnit = null;
				}
				Map.instance.cell[to.x, to.y].baseUnit = unit;
			}
		}
	}

	public static void MoveUnit(Unit unit, Coord from, Coord to)
	{
		if (instance != null)
		{
			if (from.x >= 0 && from.y >= 0 && from.x < instance.width && from.y < instance.height &&
			    to.x >= 0 && to.y >= 0 && to.x < instance.width && to.y < instance.height)
			{
				if (Map.instance.cell[from.x, from.y].unit == unit)
				{
					Map.instance.cell[from.x, from.y].unit = null;
				}
				Map.instance.cell[from.x, from.y].prevUnit = unit;
				Map.instance.cell[to.x, to.y].unit = unit;
			}
		}
	}

	public static void RemovePrevUnit(Coord from)
	{
		if (instance != null)
		{
			if (from.x >= 0 && from.y >= 0 && from.x < instance.width && from.y < instance.height)
			{
				Map.instance.cell[from.x, from.y].prevUnit = null;
			}
		}
	}

	public Coord GetHeroStartPoint()
	{
		return Map.instance.startPoint;
	}

	public Enemy[] GetMapEnemies()
	{
		List<Enemy> enemies = new List<Enemy>();
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				if (cell[i, j].baseUnit is Enemy) {
					enemies.Add(cell[i, j].baseUnit as Enemy);
				}
			}		
		}
		return enemies.ToArray();
	}

	void Awake()
	{
		instance = this;
	}
	
	void Start()
	{
		AssemblyMap();
	}

	void AssemblyMap()
	{
		if (cell == null)
			return;
		// CreateCells
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				cell[i, j].CreateCell(cell[i, j].type, i, j);
			}		
		}
		finalTeleport = cell[finalPoint.x, finalPoint.y].structureObject.GetComponent<TeleportController>();
		// Add borders
		int switcher = 0;
		for (int i = 1; i < width - 1; i++)
		{
			for (int j = 1; j < height - 1; j++)
			{
				if (cell[i, j].type != CellType.None)
				{
					for(int n = 0; n < neighbors.Length; n++)
					{
						if (cell[i + neighbors[n].x, j + neighbors[n].y].type == CellType.None)
						{
							Map.instance.LoadPrefab(i, j, neighborAngle[n], "Borders/border_0" + (switcher + 1).ToString());
							switcher = (switcher + 1) % 4;
						}
					}
				}
			}		
		}
	}

	public Coord[] FindPath(Coord from, Coord to)
	{
		ClearMapMarkers();
		MarkNearest(from, to);
		if (cell[to.x, to.y].marker >= 0)
		{
			List<Coord> path = new List<Coord>();
			Coord current = to;
			path.Add(current);
			for(int mark = cell[to.x, to.y].marker; mark > 0; mark --) {
				foreach (var neighbor in neighbors)
				{
					Coord coord = current + neighbor;
					if (cell[coord.x, coord.y].marker == mark - 1)
					{
						current = current + neighbor;
						break;
					}
				}
				path.Add(current);
			}
			return path.ToArray();
		}
		else
		{
			return null; // no path to that point
		}
	}

	void MarkNearest(Coord current, Coord to, int len = 0)
	{
		if (!IsOnFloor(current))
			return;
		if (len > 12)
			return;
		if (len != 0 && (cell[current.x, current.y].baseUnit != null || cell[current.x, current.y].bomb != null))
			return;
		if (cell[current.x, current.y].marker >= 0 && cell[current.x, current.y].marker <= len) // cell already marked as nearest
			return;
		cell[current.x, current.y].marker = len;
		if (current == to)
			return; // finded!
		// mark neighbors
		foreach (var neighbor in neighbors)
		{
			MarkNearest(current + neighbor, to, len + 1);
		}
	}

	void ClearMapMarkers()
	{
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				cell[i, j].marker = -1;
			}
		}
	}

	GameObject LoadPrefab(int x, int y, float angle, string prefabName)
	{
		GameObject element = (GameObject)Instantiate(Resources.Load("Prefabs/" + prefabName));
		element.transform.parent = transform;
		element.transform.localPosition = CoordToHex(x, y);
		element.transform.localEulerAngles = new Vector3(0, angle);
		return element;
	}
}