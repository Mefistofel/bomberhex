﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class MeshCombiner : MonoBehaviour
{
	public Material material = null;

	protected MeshFilter meshFilter;
	protected MeshRenderer meshRenderer;

	void Awake () {
		meshRenderer = gameObject.GetComponent<MeshRenderer>();
		if (meshRenderer == null)
		{
			meshRenderer = gameObject.AddComponent<MeshRenderer>();
		}
		meshFilter = gameObject.GetComponent<MeshFilter>();
		if (meshFilter == null)
		{
			meshFilter = gameObject.AddComponent<MeshFilter>();
		}
	}

	void Start()
	{
		if (material != null){
			meshRenderer.material = material;
		}
		meshFilter.mesh = CombineChildrens();
	}

	public Mesh CombineChildrens()
	{
		MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
		List<CombineInstance> combines = new List<CombineInstance>();
		for (var i = 0; i < meshFilters.Length; i++){
			if (meshFilters[i] != null && meshFilters[i].sharedMesh != null && meshFilters[i].sharedMesh.isReadable)
			{
				CombineInstance combine = new CombineInstance();
				combine.mesh = meshFilters[i].sharedMesh;
				combine.transform = meshFilters[i].transform.localToWorldMatrix;
				combines.Add(combine);
				// meshFilters[i].gameObject.SetActive(false);
				Destroy(meshFilters[i].gameObject);
			}
		}
		Mesh mesh = new Mesh();
		mesh.CombineMeshes(combines.ToArray(), true);
		return mesh;
	}
}
