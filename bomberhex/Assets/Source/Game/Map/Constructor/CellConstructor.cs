﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
[ExecuteInEditMode]
#endif
public class CellConstructor : MonoBehaviour {
	static CellConstructor singleInstance = null;
	public enum Rotation
	{
		degrees_0,
		degrees_60,
		degrees_120,
		degrees_180,
		degrees_240,
		degrees_300
	}

	public MapConstructor parent = null;
	public Rotation rotation = Rotation.degrees_0;
	public CellStructure structure = CellStructure.None;
	public CellType type = CellType.None;
	public Bonus.Type bonus = Bonus.Type.None;
	public EnemyType enemy = EnemyType.None;
	public GameObject floorObject = null;
	public GameObject structureObject = null;
	public GameObject bonusObject = null;
	public int x = 0;
	public int y = 0;

	#if UNITY_EDITOR
	void Start ()
	{
		if (parent == null)
		{
			parent = transform.GetComponentInParent<MapConstructor>();
		}
		RecreateObject();
		singleInstance = this;
	}

	CellConstructor TryToFindCellParent(Transform parent)
	{
		CellConstructor cell = parent.GetComponent<CellConstructor>();
		if (cell != null)
		{
			return cell;
		}
		if (parent != null && parent.transform.parent != null)
		{
			cell = parent.transform.parent.GetComponent<CellConstructor>();
			if (cell != null)
			{
				return cell;
			}
			return TryToFindCellParent(parent.transform.parent);
		}
		return null;
	}

	void CatchSelected()
	{
		List<Object> selectedObjects = new List<Object>(Selection.objects);
		List<Object> newObj = new List<Object>();
		int count = 0;
		for (int i = 0; i < selectedObjects.Count; i++)
		{
			if (!(selectedObjects[i] is MonoBehaviour))
			{
				return;
			}
			if (selectedObjects[i] != null)
			{
				if (selectedObjects[i] is GameObject)
				{
					GameObject obj = (GameObject)selectedObjects[i];
					CellConstructor cell = TryToFindCellParent(obj.transform);
					if (cell != null)
					{
						newObj.Add(cell.gameObject);
						count ++;
					}
				}
			}
		}
		if (count > 0)
		{
			Selection.objects = newObj.ToArray();
		}
	}

	void RecreateObject()
	{
		Vector3 position = Map.CoordToHex (x, y);
		currentPosition = position;
		transform.position = position;
		List<GameObject> childrens = new List<GameObject>();
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			childrens.Add(child.gameObject);
		}
		foreach (var child in childrens) {
			DestroyImmediate(child);
		}
		if (type != CellType.None)
		{
			floorObject = LoadPrefab("Floor/FloorTile_" + Random.Range(0, 9), 60f * Random.Range(0, 6));
		}
		if (structure != CellStructure.None && structure != CellStructure.Floor)
		{
			structureObject = LoadPrefab("Enviroment/" + structure.ToString(), (int)rotation * 60f);
		}
		if (bonus != Bonus.Type.None)
		{
			bonusObject = LoadPrefab("Bonus/Bonus" + bonus.ToString());
			bonusObject.transform.localPosition += new Vector3(0, 1, 0);
		}
		if (enemy != EnemyType.None)
		{
			bonusObject = LoadPrefab("Units/" + enemy.ToString());
			bonusObject.transform.localPosition += new Vector3(0, 0, 0);
		}
	}

	Vector3 currentPosition = Vector3.zero;
	Rotation currentRotation = Rotation.degrees_0;
	CellStructure currentStructure = CellStructure.None;
	Bonus.Type currentBonus = Bonus.Type.None;
	EnemyType currentEnemy = EnemyType.None;

	int reselectTimer = 0;

	void OnDrawGizmos ()
	{
		if (singleInstance == null)
		{
			reselectTimer++;
			if (reselectTimer == 600)
			{
				singleInstance = this;
			}
		}
		else
		{
			reselectTimer = 0;
		}
		if (singleInstance == this)
		{
			singleInstance.CatchSelected();
		}
		Vector3 position = Map.CoordToHex (x, y);
		if (currentPosition != position)
		{
			currentPosition = position;
			transform.position = position;
		}
		if (currentRotation != rotation)
		{
			currentRotation = rotation;
			RecreateObject();
		}
		if (currentStructure != structure)
		{
			currentStructure = structure;
			type = MapTileset.GetType(structure);
			RecreateObject();
		}
		if (currentBonus != bonus)
		{
			currentBonus = bonus;
			RecreateObject();
		}
		bool isSelected = false;
		GameObject[] selected = Selection.gameObjects;
		if (selected != null) {
			foreach(var obj in selected) {
				if (obj == gameObject) {
					isSelected = true;
					break;
				}
			}
		}
		if (enemy != EnemyType.None)
		{
			if (enemy != currentEnemy)
			{
				RecreateObject();
			}
			currentEnemy = enemy;
		}
//		Vector3 size = new Vector3(Map.Cell.size * 0.9f,  0.2f, Map.Cell.size * 0.9f);
//		switch (structure) {
//			case CellStructure.Floor:
//				Gizmos.color = Color.yellow;
//				break;
//			case CellStructure.StartTeleport:
//				Gizmos.color = Color.blue;
//				break;
//			case CellStructure.FinalTeleport:
//				Gizmos.color = Color.magenta;
//				break;
//			case CellStructure.ArmorLine:
//				Gizmos.color = new Color(0.95f, 0.95f, 0.95f);
//				size = new Vector3(Map.Cell.size * 0.9f,  0.3f, Map.Cell.size * 0.9f);
//				break;
//			case CellStructure.ArmorTile:
//				Gizmos.color = new Color(1.0f, 1.0f, 1.0f);
//				size = new Vector3(Map.Cell.size * 0.9f,  0.3f, Map.Cell.size * 0.9f);
//				break;
//			case CellStructure.WallLine:
//				Gizmos.color = new Color(0.95f, 0.25f, 0.25f);
//				size = new Vector3(Map.Cell.size * 0.9f,  0.3f, Map.Cell.size * 0.9f);
//				break;
//			case CellStructure.WallTile:
//				Gizmos.color = new Color(1.0f, 0.3f, 0.3f);
//				size = new Vector3(Map.Cell.size * 0.9f,  0.3f, Map.Cell.size * 0.9f);
//				break;
//			case CellStructure.TreeLine:
//				Gizmos.color = new Color(0.2f, 0.9f, 0.2f);
//				size = new Vector3(Map.Cell.size * 0.9f,  0.3f, Map.Cell.size * 0.9f);
//				break;
//			case CellStructure.TreeTile:
//				Gizmos.color = new Color(0.2f, 0.8f, 0.2f);
//				size = new Vector3(Map.Cell.size * 0.9f,  0.3f, Map.Cell.size * 0.9f);
//				break;
//			case CellStructure.None:
//				Gizmos.color = Color.black;
//				size = new Vector3(Map.Cell.size * 0.6f,  0.1f, Map.Cell.size * 0.6f);
//				break;
//		}
		bool needShowGizmo = true;
		if (parent != null)
		{
			needShowGizmo = parent.needShowGizmo;
		}
		if (needShowGizmo)
		{
			if (structure == CellStructure.None)
			{
				Vector3 size = new Vector3(Map.Cell.size * 0.6f,  0.1f, Map.Cell.size * 0.6f);
				if (isSelected)
				{
					Gizmos.DrawCube(currentPosition - new Vector3(0,size.y, 0), size * 0.6f);
				}
				else
				{
					Gizmos.DrawCube(currentPosition - new Vector3(0, size.y, 0), size);
				}
			}
		}
	}

	GameObject LoadPrefab(string prefabName, float angle = 0)
	{
		GameObject element = (GameObject)Instantiate(Resources.Load("Prefabs/" + prefabName));
		element.transform.parent = transform;
		element.transform.localPosition = Vector3.zero;
		element.transform.localEulerAngles = new Vector3(0, angle);
		return element;
	}
	#endif
}
