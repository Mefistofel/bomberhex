﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
[ExecuteInEditMode]
#endif
public class MapConstructor : MonoBehaviour
{
	[SerializeField]string levelName; // Set from Editor
	[SerializeField]string comment; // Set from Editor

	public Vector3 mapShift = Vector3.zero;

	public int width = 14;
	public int height = 14;
	public int borderWeight = 1;
	public CellConstructor[] cells = null;
	
	public bool needShowGizmo = true;

	void Awake () {
		Map.shift = mapShift;
	}

	#if UNITY_EDITOR
	
	public TextAsset levelFile = null;
	public bool needSave = false;

	public int corners = 20;
	
	void Start () {
		Map.shift = mapShift;
	}

	void DestroyCells () {
		cells = null;
		List<GameObject> childrens = new List<GameObject>();
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			childrens.Add(child.gameObject);
		}
		foreach (var child in childrens) {
			DestroyImmediate(child);
		}
	}

	[ContextMenu("Recreate")]
	public void Recreate() {
		DestroyCells();
		Map.shift = mapShift;
		// CreateCells
		cells = new CellConstructor[width * height];
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				cells[i * height + j] = Utils.Create<CellConstructor>(transform, "cell_" + i.ToString() + "_" + j.ToString());
				cells[i * height + j].parent = this;
				cells[i * height + j].x = i;
				cells[i * height + j].y = j;
				cells [i * height + j].structure = CellStructure.Floor;
				// clear borders
				if (i < borderWeight || j < borderWeight || i > width - 1 - borderWeight || j > height - 1 - borderWeight) {
					cells [i * height + j].structure = CellStructure.None;
				}
			}
		}
		// clear corners
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				if (j + (width * 0.5f - borderWeight) - i < 0)
				{
					cells [i * height + j].structure = CellStructure.None;
				}
				if (j - height + (width * 0.5f + borderWeight) - i > 0)
				{
					cells [i * height + j].structure = CellStructure.None;
				}
			}
		}
		mapShift = new Vector3(-(float)(width - 1f) * 0.5f * 0.866f, 0, -(float)(height - 1f) * 0.5f + (float)(width - 1) * 0.25f) * Map.Cell.size;
	}

	[ContextMenu("Save to file")]
	public void SaveToFile() {
		MapTileset map = new MapTileset(width, height);
		if (cells != null) {
			for(int i = 0; i < width; i++) {
				for(int j = 0; j < height; j++) {
					map[i, j] = new Cell();
					if (cells[i * height + j] != null) {
						map[i, j] = new Cell() {
							structure = cells[i * height + j].structure,
							type = cells[i * height + j].type,
							enemy = cells[i * height + j].enemy,
							angle = (int)cells[i * height + j].rotation * 60};
					}
				}
			}
		}
		map.Shift = mapShift;
		map.name = levelName;
		map.comment = comment;
		map.SaveToAsset(levelFile);
	}

	[ContextMenu("Load from file")]
	public void LoadFromFile() {
		MapTileset map = MapTileset.LoadFromAsset(levelFile);
		mapShift = map.Shift;
		width = map.width;
		height = map.height;
		DestroyCells();
		cells = new CellConstructor[width * height];
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				cells[i * height + j] = Utils.Create<CellConstructor>(transform, "cell_" + i.ToString() + "_" + j.ToString());
				cells[i * height + j].parent = this;
				cells[i * height + j].x = i;
				cells[i * height + j].y = j;
				cells [i * height + j].structure = map[i, j].structure;
				cells [i * height + j].enemy = map[i, j].enemy;
				cells [i * height + j].rotation = (CellConstructor.Rotation)(map[i, j].angle / 60);
				// clear borders
				if (i == 0 || j == 0 || i == width - 1 || j == height - 1) {
					cells [i * height + j].structure = CellStructure.None;
				}
			}
		}
	}

	void OnDrawGizmos() {
		Map.shift = mapShift;
		Vector3 TopRightCorner = Map.CoordToHex (width - 1, height - 1);
		Vector3 TopLeftCorner = Map.CoordToHex (width - 1, 0);
		Vector3 BottomRightCorner = Map.CoordToHex (0, height - 1);
		Vector3 BottomLeftCorner = Map.CoordToHex (0, 0);
		Gizmos.color = Color.green;
		Gizmos.DrawLine (TopRightCorner, TopLeftCorner);
		Gizmos.DrawLine (TopLeftCorner, BottomLeftCorner);
		Gizmos.DrawLine (BottomLeftCorner, BottomRightCorner);
		Gizmos.DrawLine (BottomRightCorner, TopRightCorner);
		if (needSave) {
			if (levelFile != null) {
				SaveToFile();
			}
			needSave = false;
		}
	}
	#endif
}
