﻿using UnityEngine;

public struct Coord
{
	public int x;
	public int y;
	
	public static Coord Zero
	{
		get
		{
			return new Coord(0, 0);
		}
	}
	
	public Coord(int nx = 0, int ny = 0)
	{
		x = nx;
		y = ny;
	}
	
	public static Coord operator +(Coord a, Coord b)
	{
		return new Coord(a.x + b.x, a.y + b.y);
	}
	
	public static Coord operator *(Coord a, int b)
	{
		return new Coord(a.x * b, a.y * b);
	}
	
	public static Coord operator *(int b, Coord a)
	{
		return new Coord(a.x * b, a.y * b);
	}
	
	public static bool operator ==(Coord a, Coord b)
	{
		return (a.x == b.x && a.y == b.y);
	}
	
	public static bool operator !=(Coord a, Coord b)
	{
		return (a.x != b.x || a.y != b.y);
	}
	
	public override bool Equals(object o)
	{
		return base.Equals(o);
	}
	public override int GetHashCode()
	{
		return base.GetHashCode();
	}
}