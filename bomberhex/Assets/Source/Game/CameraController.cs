﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
	static CameraController instance = null;

	[SerializeField]
	private Camera controlCamera; // Fill from editor or assembler

	// camera move
	Transform selfTransform;
	Vector3 needPosition = Vector3.zero;
	Vector3 oldPosition = Vector3.zero;
	float animationTimer = 0;
	float moveTime = 0.3f;

	public static float Angle
	{
		get
		{
			if (instance == null)
			{
				return 0;
			}
			return instance.transform.localEulerAngles.y;
		}
	}
	
	void Awake()
	{
		if (instance != null)
		{	// to avoid duplicate on different scenes
			Destroy(instance.gameObject);
		}
		instance = this;
		selfTransform = transform;
		needPosition = selfTransform.position;
	}

	public static void StartAnimation(Vector3 newPosition)
	{
		if (instance != null)
		{
			instance.oldPosition = instance.transform.localPosition;
			instance.needPosition = newPosition;
			instance.animationTimer = 0;
			instance.selfTransform.localPosition = newPosition;
			instance.controlCamera.transform.localPosition = new Vector3(0,0,-7f + 5f);
			Timer.Add(-0.5f, 1.5f, (anim) => 
			{
				float distance = (1f - anim) * (1f - anim);
				instance.controlCamera.transform.localPosition = new Vector3(0,0,-7f + distance * 5f);
			});
		}
	}

	public static void MoveTo(Vector3 newPosition, bool noAnimation = false)
	{
		if (instance != null)
		{
			instance.oldPosition = instance.transform.localPosition;
			instance.needPosition = newPosition;
			instance.animationTimer = instance.moveTime;
			if (noAnimation)
			{
				instance.animationTimer = 0.000001f;
				instance.Update();
			}
		}
	}

	void Update()
	{
		if (animationTimer > 0)
		{
			animationTimer -= Time.deltaTime;
			if (animationTimer < 0)
			{
				animationTimer = 0;
			}
			selfTransform.localPosition = Vector3.Lerp(needPosition, oldPosition, animationTimer / moveTime * animationTimer / moveTime);
		}
	}
}
