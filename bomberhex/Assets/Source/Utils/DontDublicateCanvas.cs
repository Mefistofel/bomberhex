﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Canvas))]
public class DontDublicateCanvas : MonoBehaviour
{
	void Awake ()
	{
		DontDestroyOnLoad(this.gameObject);
		if(BaseUI.MainCanvas != transform)
		{
			Destroy(gameObject);
		}
	}
}
