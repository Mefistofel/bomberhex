﻿using UnityEngine;
using System.Collections;

public class LookAtCamera : MonoBehaviour {

	public Transform targetCameraTransform = null;
	Transform objectTransform;

	void Start () {
		if (targetCameraTransform == null) {
			targetCameraTransform = Camera.main.transform;		
		}
		objectTransform = transform;
	}

	void Update () {
		if (targetCameraTransform != null) {
			objectTransform.rotation = targetCameraTransform.rotation;
		}
	}
}
